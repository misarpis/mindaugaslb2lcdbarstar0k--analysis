# make sure you have your eos space configured:  https://github.com/lhcb/first-analysis-steps/blob/master/09-eos-storage.md

#choose
#period = '20XXMagXXxx'

def prepareJob(year='2012', polarity = 'MagDown', type = 'Data', nfiles=None):


    if type == 'Data':
        period = year + polarity
    elif type == 'MC':
        period = type + year + polarity 
    else:
        raise NameError('Choose Data or MC')

    # get data set
    bkk_paths = {'2018MagDown'   : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST',
                 '2018MagUp'     : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST',
                 '2017MagDown'   : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST',
                 '2017MagUp'     : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST',
                 '2016MagDown'   : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/BHADRON.MDST',
                 '2016MagUp'     : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/BHADRON.MDST',
                 '2015MagDown'   : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRON.MDST',
                 '2015MagUp'     : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r1/90000000/BHADRON.MDST',
                 '2012MagDown'   : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/BHADRON.MDST',
                 '2012MagUp'     : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/BHADRON.MDST',
                 '2011MagDown'   : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/BHADRON.MDST',
                 '2011MagUp'     : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/BHADRON.MDST',

                 'MC2017MagDown' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15198000/ALLSTREAMS.MDST',
                 'MC2017MagUp' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15198000/ALLSTREAMS.MDST',

                 'MC2018MagDown' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15198000/ALLSTREAMS.DST',
                 'MC2018MagUp' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15198000/ALLSTREAMS.DST',


                 'MC2016MagDown' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15198000/ALLSTREAMS.MDST',
                 'MC2016MagUp' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15198000/ALLSTREAMS.MDST',



                 'MC2015MagDown' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15198000/ALLSTREAMS.MDST',
                 'MC2015MagUp' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15198000/ALLSTREAMS.MDST',
      
                 'MC2012MagDown' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15198000/ALLSTREAMS.DST',
                 'MC2012MagUp'   : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15198000/ALLSTREAMS.DST',

                 'MC2011MagDown' : 'MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/15198000/ALLSTREAMS.DST',
                 'MC2011MagUp'   : 'MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/15198000/ALLSTREAMS.DST',
                 
                 
 }


    origfile = 'maketuples.py'
    GenFileName = "maketuples_ganga_" + period + ".py"
    outputlines=[]

    file = open(origfile, "r")
    line = file.readline()
    while line:
        outputlines.append(line+'\n')
        line = file.readline()
    file.close()
    
    GENFILE = open(GenFileName,"w")
    GENFILE.write("MyOptions = ['" + type + "', '" + polarity + "', '" + year + "']\n")
    GENFILE.writelines(outputlines)
    GENFILE.close()
    
    print "period: " + period

    
    
    j=Job()
    j.name = 'Lb2LcDKst' + period
    print "Job name: " + j.name
    
    opts = []

    opts.append(GenFileName)

#############################################################################
####For the first time you run within a folder use only these two lines######

    #j.application = prepareGaudiExec('DaVinci','v44r10p2', myPath='./')
    #j.application.platform='x86_64-slc6-gcc62-opt'

#####For all subsequent times within the same folder use only these 2 lines## 

    j.application = GaudiExec()
    j.application.directory = "./DaVinciDev_v44r10p2"

    print "Using ", j.application

##############################################################################

    j.application.options = opts
    print "with ", j.application.options
    
    dataset = BKQuery(bkk_paths[period]).getDataset()
    j.inputdata=dataset[0:nfiles]
  

    
    j.backend = Dirac()
    
    j.splitter = SplitByFiles(filesPerJob = 40, ignoremissing = True)
    
    # configure the output location
    if ('MC' in period) :
        output_name = 'MC_Lb2LcDKst.root'
    else :
        output_name = 'Lb2LcDKst.root'
        
    j.outputfiles = [MassStorageFile ( compressed = False ,
                                       namePattern = output_name ,
                                       outputfilenameformat = 'B2Dh_D2KKpipi/'+period+'/{jid}_{sjid}_{fname}')]
 


    return j
