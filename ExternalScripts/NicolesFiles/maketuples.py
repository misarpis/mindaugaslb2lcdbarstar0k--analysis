
# if you want to run locally
LocalRun = True
name = 'Lb2LcD0K'
# job options:
if LocalRun:
    MyOptions = ["Data", "MagDown", "2018"]

##############################################################
# set the stripping configuration
print "!!!!!!Job info!!!!!!"
print "MyOptions:"
for i in MyOptions:
    print i


stripping_line = "X2LcD0KD02KPiBeauty2CharmLine"
MC_name = "X2LcD0KD02KPiBeauty2CharmLine"

decay_data = '(B0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(D0 -> ^K+ ^pi-) ^K-) || (B0 -> ^(Lambda_c~- -> ^p~- ^K+ ^pi-) ^(D0 -> ^K- ^pi+) ^K+)' 

Decay_MC = '(B0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(D0 -> ^K+ ^pi-) ^K-) || (B0 -> ^(Lambda_c~- -> ^p~- ^K+ ^pi-) ^(D0 -> ^K- ^pi+) ^K+)' 

tesLoc = ""

if "Data" in MyOptions:
    tesLoc = "Phys/" + stripping_line + "/Particles"  # don't preceed by '/' for uDST
elif "MC" in MyOptions:
    tesLoc = "Phys/" + stripping_line + "/Particles"
else:
    raise NameError('Choose type Data or MC')
    

print "Looking for events in " + tesLoc

###############################################################################
# Imports
################################################################################
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *

from Configurables import DaVinci, EventTuple 
from Configurables import FilterDesktop, SubstitutePID

from PhysSelPython.Wrappers import Selection
from PhysSelPython.Wrappers import SelectionSequence
from PhysSelPython.Wrappers import DataOnDemand

from Configurables import LoKi__Hybrid__EvtTupleTool
from Configurables import LoKi__Hybrid__TupleTool

if "MC" in MyOptions:
  from Configurables import MCTupleToolKinematic, MCTupleToolHierarchy, MCDecayTreeTuple

################################################################################
# SubstitutePID
################################################################################

StrippingSels = [DataOnDemand(Location=tesLoc)]

subs=SubstitutePID(
    'MakeKstbar',
    Code="DECTREE('[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC')",
    Substitutions = {
    'Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (Meson -> X+ ^pi-)':'K-',
    'Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (Meson -> ^K+ X-)':'pi+',
    
    'Lambda_b~0 -> (Lambda_c~- -> p~- K+ pi-) (D+ -> K- pi+ pi+) (Meson -> X- ^pi+)':'K+',
    'Lambda_b~0 -> (Lambda_c~- -> p~- K+ pi-) (D+ -> K- pi+ pi+) (Meson -> ^K- X+)':'pi-'}
    )

selSub = Selection(
    'SubKstbar_Sel',
    Algorithm=subs,
    RequiredSelections=StrippingSels
    )

#alg = FilterDesktop('SelFilterFor{}B2D'.format(name))
#    # apply the PID cut only on data
#if "Data" in MyOptions :
#    alg.Code = 'CHILD(PROBNNK, 1, 1) > 1.0'
#else :
#    alg.Code = 'ALL'
#reqSels = [selSub]
#sel = Selection('Sel' + name, Algorithm=alg, RequiredSelections=reqSels)


selseq = SelectionSequence('SelSubst', TopSelection=selSub)


if ("2011" in MyOptions) or ("2012" in MyOptions):
    print "Using Run1 trigger cuts"
    hlt1code=" HLT_PASS_RE ('Hlt1TrackAllL0Decision')"
else:
    print "Using Run2 trigger cuts"
    hlt1code=" HLT_PASS_RE ('Hlt1TrackMVADecision') | HLT_PASS_RE ('Hlt1TwoTrackMVADecision')"

from PhysConf.Filters import LoKi_Filters
filter_ = LoKi_Filters(
     HLT1_Code     = hlt1code,
     HLT2_Code     = " HLT_PASS_RE ('Hlt2Topo.*Decision') " ,
     STRIP_Code="HLT_PASS_RE('StrippingLb2LcDKstBeauty2CharmLineDecision')"
     )



map_data = {
    "Lb" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Lc" : "[Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Dm" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^(D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Kst" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) ^(K*(892)0 -> K- pi+)]CC",

    "Lc_p" : "[Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Lc_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Lc_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",

    "Dm_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> ^K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Dm_pim1" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ ^pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Dm_pim2" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- ^pi-) (K*(892)0 -> K- pi+)]CC",

    "Kst_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> ^K- pi+)]CC",
    "Kst_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- ^pi+)]CC",
}

map_MC = {
    "Lb" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    "Lc" : "[Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    "Dm" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^(D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    "Kst" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) ^(K*(892)0 -> K+ pi-)]CC",

    "Lc_p" : "[Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    "Lc_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    "Lc_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",

    "Dm_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> ^K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    "Dm_pim1" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ ^pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    "Dm_pim2" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- ^pi-) (K*(892)0 -> K+ pi-)]CC",

    "Kst_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> ^K+ pi-)]CC",
    "Kst_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ ^pi-)]CC",
}

triglist = [
        
        #L0 lines
        "L0CALODecision",
        "L0ElectronDecision",
        #"L0GlobalDecision",
        "L0HadronDecision",
        "L0MuonDecision",
        "L0PhotonDecision",
        
        "Hlt1TrackAllL0Decision",     
        
        #HLT2 lines
        # Run1 lines
        "Hlt2Topo2BodyBBDTDecision",
        "Hlt2Topo2BodySimpleDecision",
        "Hlt2Topo3BodyBBDTDecision",
        "Hlt2Topo3BodySimpleDecision",
        "Hlt2Topo4BodyBBDTDecision",
        "Hlt2Topo4BodySimpleDecision",
                      
        'Hlt2Topo2BodyDecision',
        'Hlt2Topo3BodyDecision',
        'Hlt2Topo4BodyDecision',
        

        "Hlt1TrackMVADecision" ,
        "Hlt1TwoTrackMVADecision"
        ]

################################################################################
# Config

if "MC" in MyOptions:
  #DaVinci().InputType = 'DST'
  rootInTES = '/Event/AllStreams'
  DaVinci(RootInTES=rootInTES, InputType='MDST')

if "Data" in MyOptions:
  rootInTES = '/Event/Bhadron'
  DaVinci(RootInTES=rootInTES, InputType='MDST')

print "Input file is " +   DaVinci().InputType
if "Data" in MyOptions:
    print "RootInTES is " + DaVinci().RootInTES
################################################################################
#DecayTreeTuple

def decay_tree_tuple(name, tuple_input, decay, map):
    """Create simple DecayTree"""

    from Configurables import TupleToolDecayTreeFitter
    from Configurables import TupleToolTrackIsolation 
    from Configurables import TupleToolSelResults
    from Configurables import TupleToolProtoPData
    from Configurables import TupleToolTrackInfo
    from Configurables import TupleToolRecoStats
    from Configurables import TupleToolKinematic
    from Configurables import TupleToolGeometry
    from Configurables import TupleToolCaloHypo
    from Configurables import TupleToolTagging
    from Configurables import TupleToolDecay
    from Configurables import TupleToolPid
        
    tpl = DecayTreeTuple('{}DTTuple'.format(name))
    
    tpl.Inputs = [tuple_input]
    tpl.addBranches(map)
    tpl.ToolList = [
        'TupleToolKinematic',
        'TupleToolPid',
        'TupleToolANNPID',
        'TupleToolGeometry',
        'TupleToolPrimaries',
        "TupleToolPropertime",
        "TupleToolRecoStats",##

       'TupleToolTrackInfo',
       'TupleToolEventInfo',   #Event infos
       'TupleToolAngles',
       "LoKi::Hybrid::EvtTupleTool/LoKiEvent"
    ]

    tpl.addTool(TupleToolGeometry,name="TupleToolGeometry")
    tpl.TupleToolGeometry.Verbose = True
    
    tpl.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
    tpl.TupleToolTrackInfo.Verbose = True
    
    tpl.addTool(TupleToolPid,name="TupleToolPid")
    tpl.TupleToolPid.Verbose = True

    #adding more variables to the output ntuple
    LoKiTool = tpl.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
    
    LoKiTool.Variables = {
      "ETA"        : "ETA"
      }

    #adding MC-truth, only for MC samples
    if "MC" in MyOptions:
        print "Adding truth info"
        MCTruth = tpl.addTupleTool("TupleToolMCTruth")
        
        MCTruth.ToolList += [
            'MCTupleToolKinematic',    #kinematic generated variables
            'MCTupleToolHierarchy',    #generated informations about parent and grand-parent
            "MCTupleToolAngles",       #generated decay angles
            #"LoKi::Hybrid::MCTupleTool/LokiTool"   #to add more generated variables in the tuple
            ]
        
        tpl.addTupleTool("TupleToolMCBackgroundInfo")
        
        MCTruth.addTool(MCTupleToolKinematic())
        MCTruth.MCTupleToolKinematic.Verbose = True
  
    #add TIS/TOS trigger variables
    from Configurables import TupleToolTISTOS, TupleToolTrigger, TriggerTisTos

    #TISTOSTool
    TISTOSTool = tpl.addTupleTool('TupleToolTISTOS')
    TISTOSTool.VerboseL0   = True
    TISTOSTool.VerboseHlt1 = True
    TISTOSTool.VerboseHlt2 = True
    TISTOSTool.TriggerList = triglist[:]
    TISTOSTool.TIS = True
    TISTOSTool.TOS = True
   
    
    #TriggerTool
    TriggerTool = tpl.addTupleTool("TupleToolTrigger")
    TriggerTool.VerboseL0   = True
    TriggerTool.VerboseHlt1 = True
    TriggerTool.VerboseHlt2 = True
    TriggerTool.TriggerList = triglist[:]
    
    tpl.Decay = decay
    return tpl




################################################################################

#MCDecayTreeTuple
def decay_tree_tuple_Full_MC(name, decay):
  
  
  mc_tpl = MCDecayTreeTuple("MCDecayTreeTuple")
  mc_tpl.addBranches(map)
  mc_tpl.ToolList = [
      "MCTupleToolHierarchy",         #generated informations about parent and grand-parent
      #"LoKi::Hybrid::MCTupleTool",    #to add more variables in the tuple
      "TupleToolEventInfo",
      "MCTupleToolAngles"         #generated decay angles
      ]
  
  mc_tpl.addTupleTool("MCTupleToolKinematic").Verbose = True
  
  #add TIS/TOS trigger variables
  from Configurables import TupleToolTrigger, TriggerTisTos
  
  TriggerTool = mc_tpl.addTupleTool("TupleToolTrigger/TriggerTool")
  TriggerTool.VerboseL0   = True
  TriggerTool.VerboseHlt1 = True
  TriggerTool.VerboseHlt2 = True
  TriggerTool.TriggerList = triglist[:]
  mc_tpl.Decay = decay

  return mc_tpl

################################################################################
# Make selection
################################################################################

tuple_input = ""
decay = ""
map = ""

if "MC" in MyOptions:
    tuple_input = tesLoc
    decay = decay_MC
    map = map_MC

if "Data" in MyOptions:
    tuple_input = selseq.outputLocation()
    decay = decay_data
    map = map_data

# make the tuple
tpl = decay_tree_tuple(name, tuple_input, decay, map)

# tuple with informations from MC generated signals
if "MC" in MyOptions:
  mctpl = decay_tree_tuple_Full_MC(name, decay)

################################################################################
#
################################################################################

# Add decay tree fitter
from Configurables import TupleToolDecayTreeFitter,TupleToolDecay

tpl.Lb.addTupleTool("TupleToolDecayTreeFitter/Cons")
tpl.Lb.Cons.constrainToOriginVertex = True
tpl.Lb.Cons.Verbose = True
tpl.Lb.Cons.daughtersToConstrain = [ "Lambda_c+","D-" ]
tpl.Lb.Cons.UpdateDaughters = True

tpl.Lb.addTupleTool("TupleToolDecayTreeFitter/ConsLb")
tpl.Lb.ConsLb.constrainToOriginVertex = True
tpl.Lb.ConsLb.Verbose = True
tpl.Lb.ConsLb.daughtersToConstrain = [ "Lambda_b0", "Lambda_c+", "D-"]
tpl.Lb.ConsLb.UpdateDaughters = True

################################################################################
seq = GaudiSequencer('My{}DTTupleSeq'.format(name))

if "Data" in MyOptions:
    seq.Members += [selseq.sequence()]
    #seq.Members += [selSeq.sequence()]

seq.Members += [tpl]

print "Added tpl "
#add the MCDecayTreeTuple
if "MC" in MyOptions:
    #seq.Members += [mctpl]
    print "Added mctpl "
  


################################################################################

# set of detector conditions for MC
if "MC" in MyOptions:
  DaVinci().Simulation = True
  DaVinci().Lumi = False



  if "2017" in MyOptions:
      DaVinci().DataType = '2017'     
      DaVinci().DDDBtag = 'dddb-20170721-3'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20180411-vc-md100'          
      elif "MagUp" in MyOptions:
          DaVinci().CondDBtag = 'sim-20180411-vc-mu100'
      else :
        raise NameError('Choose MagDown or MagUp')


  elif "2018" in MyOptions:
      DaVinci().DataType = '2018'     
      DaVinci().DDDBtag = 'dddb-20171030-3'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20180411-vc-md100'          
      elif "MagUp" in MyOptions:
          DaVinci().CondDBtag = 'sim-20180411-vc-mu100'
      else :
        raise NameError('Choose MagDown or MagUp')

  elif "2016" in MyOptions:
      DaVinci().DataType = '2016'     
      DaVinci().DDDBtag = 'dddb-20170721-3'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20170721-2-vc-md100'          
      elif "MagUp" in MyOptions:
          DaVinci().CondDBtag = 'sim-20170721-2-vc-mu100'
      else :
        raise NameError('Choose MagDown or MagUp')
            
              
  elif "2015" in MyOptions:
      DaVinci().DataType = '2015'      
      DaVinci().DDDBtag = 'dddb-20170721-3'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20161124-vc-md100'          
      elif "MagUp" in MyOptions:
          DaVinci().CondDBtag = 'sim-20161124-vc-mu100'
      else :
        raise NameError('Choose MagDown or MagUp')
                              
  elif "2012" in MyOptions:
      
      DaVinci().DataType = '2012'      
      DaVinci().DDDBtag = 'dddb-20170721-2'
    
      if "MagDown" in MyOptions:
         DaVinci().CondDBtag = 'sim-20160321-2-vc-md100'         
      elif "MagUp" in MyOptions:             
              DaVinci().CondDBtag = 'sim-20160321-2-vc-mu100'              
      else :
        raise NameError('Choose MagDown or MagUp')        
    
  elif "2011" in MyOptions:
      
      DaVinci().DataType = '2011'      
      DaVinci().DDDBtag = 'dddb-20170721-1'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20160614-1-vc-md100'         
      elif "MagUp" in MyOptions:
              DaVinci().CondDBtag = 'sim-20160614-1-vc-mu100'             
      else :
        raise NameError('Choose MagDown or MagUp')        

  else:
      raise NameError('Choose 20XX as Datatype')
  
  print "Datatype" + DaVinci().DataType
  print "DDDBtag" + DaVinci().DDDBtag
  print "CondDBtag" + DaVinci().CondDBtag

# set of detector conditions for data
if "Data" in MyOptions:
  DaVinci().Simulation = False
  DaVinci().Lumi = True
  
  if "2011" in MyOptions:
    DaVinci().DataType = '2011'
  
  elif "2012" in MyOptions:
    DaVinci().DataType = '2012'

  elif "2015" in MyOptions:
    DaVinci().DataType = '2015'
        
  elif "2016" in MyOptions:
    DaVinci().DataType = '2016'

  elif "2017" in MyOptions:
    DaVinci().DataType = '2017'

  elif "2018" in MyOptions:
    DaVinci().DataType = '2018'

  else:
    raise NameError('Choose 20XX as Datatype')
  
  print "Datatype" + DaVinci().DataType
  
#some general job options
if LocalRun :
    DaVinci().EvtMax = 10000
else:
    DaVinci().EvtMax = -1

DaVinci().SkipEvents = 0
#DaVinci().ErrorMax = 100
DaVinci().PrintFreq = 10000

DaVinci().EventPreFilters = filter_.filters("FILTERS")
#DaVinci().appendToMainSequence([selSeq])
DaVinci().appendToMainSequence([seq])

if "Data" in MyOptions:
  DaVinci().TupleFile = 'Lb2LcD0.root'

if "MC" in MyOptions:   
    DaVinci().TupleFile = 'MC_Lb2LcD0K.root'

    #to write variables from all simulated MC signal particles
    DaVinci().UserAlgorithms = [mctpl]

print "Making " + DaVinci().TupleFile
print "!!!!!!Job info!!!!!!"


################################################################################
# Run on some eos data located at CERN
################################################################################



if LocalRun :
  if "MC" in MyOptions:  
      EventSelector().Input = [
          "DATAFILE='00088331_00000018_7.AllStreams.mdst'  TYP='POOL_ROOTTREE' Opt='READ'",
          ]
  if "Data" in MyOptions:
      EventSelector().Input = [
          "DATAFILE='00077434_00031037_1.bhadron.mdst'  TYP='POOL_ROOTTREE' Opt='READ'",
          ]
      
################################################################################
