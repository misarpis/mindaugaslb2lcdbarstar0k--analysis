# for this to work you need to check out this repository:
# https://gitlab.cern.ch/sneubert/DfromBBDTs
# make sure you chave yuor eos space configured:  https://github.com/lhcb/first-analysis-steps/blob/master/09-eos-storage.md

#choose
#period = '2012MagDown'
#period = '2012MagUp'
#period = '2011MagDown'
#period = '2011MagUp'

#period = 'MC2012MagDown'
#period = 'MC2012MagUp'
#period = 'MC2011MagDown'
#period = 'MC2011MagUp'

def prepareJob(period='2018MagDown',nfiles=2):

    # get data set
    bkk_paths = {'2018MagDown'   : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST',
                 '2018MagUp'     : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST',
                 '2017MagDown'   : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST',
                 '2017MagUp'     : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST',
                 '2016MagDown'   : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/BHADRON.MDST',
                 '2016MagUp'     : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/BHADRON.MDST',
                 '2015MagDown'   : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRON.MDST',
                 '2015MagUp'     : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r1/90000000/BHADRON.MDST',
                 '2012MagDown'   : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/BHADRON.MDST',
                 '2012MagUp'     : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/BHADRON.MDST',
                 '2011MagDown'   : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/BHADRON.MDST',
                 '2011MagUp'     : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/BHADRON.MDST',

                 'MC2016MagDown_ReDecay' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09d-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2016MagUp_ReDecay'   : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09d-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2016MagDown_forReDecayValidation' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09d/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2016MagUp_forReDecayValidation'   : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09d/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2018MagDown' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2018MagUp'   : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2017MagDown' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2017MagUp'   : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2016MagDown' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2016MagUp'   : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2015MagDown' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2015MagUp'   : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2012MagDown' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09b/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2012MagUp'   : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09b/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2011MagDown' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09b/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2011MagUp'   : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09b/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 
                 'MC2012MagDown_old' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2012MagUp_old'   : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15396000/ALLSTREAMS.DST',
                 'MC2011MagDown_old' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST',  
                 'MC2011MagUp_old'   : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/15396000/ALLSTREAMS.DST'}

    dataset = BKQuery(bkk_paths[period]).getDataset()
    
    j=Job()
    j.name = 'LcD0K_' + period

    # build the period string:
    # remove "ReDecay" and "_forRedecayValidation",
    # they have same options of the standard simulation 
    period_string = 'setup' + period + '.py'

    for to_remove in ["_ReDecay", "_forRedecayValidation"] :
        if to_remove in period_string :
            period_string = period_string.replace(to_remove, '')

    # build the options
    opts = []
    
    opts.append(period_string);
    opts.append('mvaLb2LcD0K.py')

    #if you don't want to restrip the MC, then use this vanilla-DaVinci installation
    application_directory = "/afs/cern.ch/user/m/misarpis/DaVinci_v50r3"
    platform = "x86_64-centos7-gcc8-opt"
    
    #some more specific DaVinci installations, to restrip the MC
    if ("MC2016" in period or "MC2015" in period) and ("restrip" in period):
        application_directory = "/eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/code_analysis/DaVinci_v41r4p4"
        platform = "x86_64-slc6-gcc49-opt"
        
    #new lb-* formalism
    myApp = GaudiExec()
    myApp.options = opts
    myApp.directory = application_directory
    j.application = myApp
    j.application.platform = platform

    #old SetupProject formalism
    #j.application = DaVinci(version='v42r2')
    #j.application.platform='x86_64-slc6-gcc49-opt'
    #j.application.optsfile = opts
    
    j.inputdata=dataset[0:nfiles]
    
    j.backend = Dirac()
    #j.backend = LSF( queue = '1nd' )
    
    j.splitter = SplitByFiles(filesPerJob = 40, ignoremissing = True)
    
    # configure the output location
    if ('MC' in period) :
        output_name = 'MC_LcD0K.root'
    else :
        output_name = 'bdt_LcD0K.root'
        
    #j.outputfiles = [MassStorageFile ( compressed = False ,
    #                                   namePattern = output_name ,
    #                                   outputfilenameformat = 'Lb2LcD0K/'+period+'/{jid}_{sjid}_{fname}')]

    j.outputfiles = [DiracFile (namePattern = output_name)]

    # let's hope for the best
    j.do_auto_resubmit = True
    
    return j
