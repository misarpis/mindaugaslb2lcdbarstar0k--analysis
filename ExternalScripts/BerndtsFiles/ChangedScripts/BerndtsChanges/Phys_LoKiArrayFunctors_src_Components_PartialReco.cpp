// $Id:$ 
// ============================================================================
#include "LoKi/DictTransform.h"
// ============================================================================
#include <iostream>
//#include <boost/foreach.hpp>
// ============================================================================
#include "TLorentzVector.h" 
#include "TVector.h"
#include "TVector3.h"
#include "TRotation.h"
#include "TMath.h"
#include "LoKi/Child.h"
#include "Math/GenVector/Boost.h"
#include "Math/GenVector/AxisAngle.h"
#include "Math/Vector4D.h"
// ============================================================================
typedef std::map<std::string,std::string> optmap;
//typedef LoKi::BasicFunctors<const LHCb::Particle*>::FunctionFromFunction Fun;
//using namespace LoKi;
//using namespace LoKi::Types;
//using namespace LoKi::Cuts;
//typedef LoKi::Particles::ChildFunction CHILD;
//typedef LoKi::Particles::Mass M;
//typedef LoKi::Particles::Energy E;
//typedef LoKi::Particles::Momentum P;
// ============================================================================
/** @class PartialReco
 *  Implements the actual partial reconstruction Candidate transform to be used with
 *  DictTransform for the Lb2LcD0Kg/pi0
 *  fulfilling the following policy
 *  TransForm::Init(const std::map<std::string,std::string>& options, ostream& info)
 *  TransForm::operator()(const LHCb::Particle* in) const
 *
 *  The Partial Reco Candidate Transform will return the source particle extended with the missing photon/pion
 *
 *  @author Bernd Mumme
 *  @date   2018-09-05
 */

class ConeMethodPartialRecoTransform
{
private:
  
  bool m_debug;
  
public:

  ConeMethodPartialRecoTransform():m_debug(false)
  {
  }
  
  bool Init(const optmap& options, std::ostream& info, const bool debug = false)
  {
    m_debug = debug;
    if (m_debug) info << "Initialising Partial Reconstruction" << std::endl;
    info << "Starting Reconstruction" << std::endl;
    return true; //true signals success
    
  }
  
  LHCb::Particle* operator()(const LHCb::Particle* in) const;

};




///---------------------------------------------------------------------------------------------------

LHCb::Particle*
ConeMethodPartialRecoTransform::operator()(const LHCb::Particle* in) const
{
  auto p_out = new LHCb::Particle(*in);
  //const Double_t somethreshold = ;
  std::cout << p_out << std::endl;
  std::cout << in << std::endl;
  std::cout << "Starting particle momentum (Lb)" << std::endl;
  std::cout << p_out->momentum() << std::endl;
  std::cout << p_out->measuredMass() << std::endl;
  
  std::cout << "Actually Starting Reconstruction" << std::endl;
  
  //auto lbId_input = p_out->particleID();
 
    //create all needed particles and assign respective particle ID's
    LHCb::Particle* Gamma = new LHCb::Particle();
    std::cout << Gamma << std::endl; 
    int gammaId = 22;
    Gamma->setParticleID(LHCb::ParticleID(gammaId));

    LHCb::Particle* Pion = new LHCb::Particle();
    std::cout << Pion << std::endl;
    int pionId = 111;
    Pion->setParticleID(LHCb::ParticleID(pionId));
    
    LHCb::Particle* Lb = new LHCb::Particle();
    std::cout << Lb << std::endl;
    int lbId = 5122;//std::copysign(5122, lbId_input.pid());
    Lb->setParticleID(LHCb::ParticleID(lbId));
    
    LHCb::Particle* Dst = new LHCb::Particle();
    std::cout << Dst << std::endl;
    int dstId = -423;
    Dst->setParticleID(LHCb::ParticleID(dstId));
   

    //Lb = p_out, assign daughters
    LHCb::Particle* Lc = LoKi::Child::child(p_out,1);
    LHCb::Particle* D0 = LoKi::Child::child(p_out,2);
    LHCb::Particle* K = LoKi::Child::child(p_out,3);
    

    //get unit vector of lambda_b and its mass
    const auto p_lb = p_out->momentum();
    
    std::cout << "Lb momentum:" << std::endl << p_lb << std::endl << std::endl;
    
    auto unit_lb = p_lb.Vect().Unit();
    Double_t unit_lb_x = unit_lb.X();
    Double_t unit_lb_y = unit_lb.Y();
    Double_t unit_lb_z = unit_lb.Z();
    ROOT::Math::XYZVector unit_lb_root(unit_lb_x, unit_lb_y, unit_lb_z);
    
    Double_t m_lb = p_lb.M();
    
    std::cout << "Lb mass: " << m_lb << std::endl << std::endl;
    
    //lambda_c threevector and energy
    
    auto p_lc = Lc->momentum();
    std::cout << "Lc momentum:" << std::endl;
    std::cout << p_lc << std::endl << std::endl << std::endl;
    

    //Double_t px_lc = p_lc.px();
    //Double_t py_lc = p_lc.py();
    //Double_t pz_lc = p_lc.pz();
    //Double_t E_lc = p_lc.E();
    
    
    //get D0 momentum and its momentum covariance matrix as an estimate for the Dst cov matrix
    auto p_d0 = D0->momentum();
    std::cout << "D0 momentum covariance matrix:" << std::endl;
    std::cout << D0->momCovMatrix() << std::endl << std::endl;
    auto d0_cov = D0->momCovMatrix();
    //Double_t px_d0 = p_d0.px();
    //Double_t py_d0 = p_d0.py();
    //Double_t pz_d0 = p_d0.pz();
    //Double_t E_d0 = p_d0.E();
    //ROOT::Math::LorentzVector grandchild (px_d0,py_d0,pz_d0, E_d0);

    //std::cout << "D0 momentum:" << std::endl;
    //std::cout << px_d0 << ", " << py_d0 << ", " << pz_d0 << ", " << E_d0 << std::endl;
    
    //const LHCb::Particle Dst = get_Dst( p_out );
    
    //set the Dst vertex to the Lb vertex
    LHCb::Vertex *lb_endVertex = p_out->endVertex()->clone();
    const LHCb::Vertex *vd0 = lb_endVertex ;
    Dst->setEndVertex(vd0) ;
    
    //Add daughters to Dst
    Dst->addToDaughters( D0 );
    Dst->addToDaughters( Gamma ); //either or
    //Dst->addToDaughters( Pion ); //either or
    //Dst->setMomentum( D0->momentum() + Gamma->momentum() );
    //Dst->setMomentum( D0->momentum() + Pion->momentum() );

    //Dst mass
    Dst->setMeasuredMass(2007);
    
    Double_t m_Dst = Dst->measuredMass();
    
    std::cout << "Dst mass:" << std::endl;
    std::cout << m_Dst << std::endl;
    std::cout << "Dst momentum:" << std::endl;
    std::cout << Dst->momentum() << std::endl;
    
    //Kaon threevector and energy
    auto p_K = K->momentum();
    Double_t px_K = p_K.px();
    Double_t py_K = p_K.py();
    Double_t pz_K = p_K.pz();
    Double_t E_K = p_K.E();
    
    std::cout << "K momentum:" << std::endl;
    std::cout << px_K << ", " << py_K << ", " << pz_K << ", " << E_K << std::endl;

    //fourvector of lambda_c + Kaon
    //TLorentzVector kaon4 (px_K, py_K, pz_K, E_K);
    //TLorentzVector lambdac4 (px_lc, py_lc, pz_lc, E_lc);
    
    //construct combined system of Kaon and Lc
    auto otherchildren = p_K + p_lc;

    //missing particle mass
    Pion->setMeasuredMass(134.977);
    Gamma->setMeasuredMass(0);
    //ROOT::Math::PxPyPzEVector zero_vec (0, 0, 0, 0);
    //Pion->setMomentum(zero_vec);
    //Gamma->setMomentum(zero_vec);
    Double_t m_gamma = Gamma->measuredMass();
    Double_t m_pion = Pion->measuredMass();
    
    std::cout << "Gamma and Pion masses:" << std::endl;
    std::cout << m_gamma << ", " << m_pion << std::endl;
    std::cout << "Gamma and Pion momenta:" << std::endl;
    std::cout << Gamma->momentum() << ", " << Pion->momentum() << std::endl;
    
    //insert Fabrice's function here, returns partially reconstructed Dst (PL_child1_reco_bestFit, a fourvector)
    
    std::cout << "Let's calculate some stuff" << std::endl;
    

      //Get grandChild momemtum
      //TVector3 grandChild_boostVector = grandchild.BoostVector();
      auto grandChild_boostVector = p_d0.BoostToCM();
    
  
      //Go to rest frame of grandChild
      ROOT::Math::Boost grandchild_boost(-grandChild_boostVector);
      ROOT::Math::Boost neg_grandchild_boost(grandChild_boostVector);
      otherchildren = grandchild_boost(otherchildren);
      
      p_d0 = grandchild_boost(otherchildren);

      //m2 list
      Double_t m2_Mother = pow(m_lb,2);
      Double_t m2_other_children = otherchildren.M2();
      Double_t m2_child1 = pow(m_Dst, 2);
      Double_t m_grandChild = p_d0.M();
      Double_t m2_grandChild = p_d0.M2();
      Double_t m2_X = pow(m_gamma, 2);
      //either ^^ or Double_t m2_X = pow (m_pion, 2);

      ROOT::Math::PxPyPzEVector PL_child1_reco (1., 1., 1., m_Dst);

     //Calculate magnitude of p_child1
      Double_t p_child1_reco_mag = sqrt(pow((m2_child1 - m2_grandChild - m2_X)/
                    (2.*m_grandChild),2) - m2_X);

      //initialize p_child1_reco = p_other_children
      PL_child1_reco.SetPx(otherchildren.Px());
      PL_child1_reco.SetPy(otherchildren.Py());
      PL_child1_reco.SetPz(otherchildren.Pz());
      PL_child1_reco.SetE(m_Dst);
      ROOT::Math::PtEtaPhiE4D<double> PL_child1_reco_spherical(PL_child1_reco.Coordinates());
      PL_child1_reco_spherical.SetPt(p_child1_reco_mag);

      //calculate cosTheta, angle between p_child1 and p_other_children
      Double_t child1_E_reco = PL_child1_reco_spherical.E();
      Double_t other_children_E = otherchildren.E();
      Double_t p_other_children_mag = otherchildren.M();
      Double_t cosTheta = (2.*child1_E_reco*other_children_E - m2_Mother 
               + m2_child1 + m2_other_children)
        /(2.*p_child1_reco_mag*p_other_children_mag);

      //Rotate PL_child1_reco of the angle theta wrt p_other_childre
      auto ortho_x = std::abs(otherchildren.Vect().x());
      auto ortho_y = std::abs(otherchildren.Vect().y());
      auto ortho_z = std::abs(otherchildren.Vect().z());
      auto ortho_xx = otherchildren.Vect().x();
      auto ortho_yy = otherchildren.Vect().y();
      auto ortho_zz = otherchildren.Vect().z();
      auto ortho_e = otherchildren.E();
      ROOT::Math::PxPyPzEVector ortho_otherChildren;
      if (ortho_x < ortho_y) {
	ortho_x < ortho_z ? ortho_otherChildren =  ROOT::Math::PxPyPzEVector(0, ortho_zz, -ortho_yy, ortho_e) : ortho_otherChildren = ROOT::Math::PxPyPzEVector(ortho_yy, -ortho_xx, 0, ortho_e);
	}
      else {
	ortho_y < ortho_z ? ortho_otherChildren = ROOT::Math::PxPyPzEVector(-ortho_zz, 0, ortho_xx, ortho_e) : ortho_otherChildren = ROOT::Math::PxPyPzEVector(ortho_yy, -ortho_xx, 0, ortho_e);
	} 
      // auto ortho_other_children = (otherchildren.Vect()).Orthogonal();
      ROOT::Math::AxisAngle rotation(ortho_otherChildren.Vect(), TMath::ACos(cosTheta));
      PL_child1_reco = rotation(ortho_otherChildren);

      //Initialize parameters for determination of phi angle
      auto PL_child1_reco_bestFit = PL_child1_reco;
      Double_t best_coplanar = 1.e+10;
      Double_t coplanar = 0.;

      for(int i=0 ; i<3601 ; i++) { //0.1 steps * 3600 = 360°

        //Rotate PL_child1_reco of 1° arround p_other_children axis
    //     PL_child1_reco.Rotate(0.1*TMath::Pi()/180., otherchildren.Vect());
	ROOT::Math::AxisAngle rotation2(otherchildren.Vect(), 0.1*TMath::Pi()/180.);
	PL_child1_reco = rotation2(otherchildren);
        //Go to Lab frame
        otherchildren = neg_grandchild_boost(otherchildren);
        PL_child1_reco = neg_grandchild_boost(PL_child1_reco);
    //     otherchildren.Boost(grandChild_boostVector);    
    //     PL_child1_reco.Boost(grandChild_boostVector);

        //Test if better coplanar then before
        coplanar = (otherchildren.Vect()).Cross((PL_child1_reco.Vect())).Dot(unit_lb_root);

        if(abs(coplanar) < best_coplanar) {
          best_coplanar = abs(coplanar);
          PL_child1_reco_bestFit = PL_child1_reco;
        }

    //     //Go to grandChild rest frame
       otherchildren = grandchild_boost(otherchildren);
       PL_child1_reco = grandchild_boost(PL_child1_reco);
    //     otherchildren.Boost(-grandChild_boostVector);    
    //     PL_child1_reco.Boost(-grandChild_boostVector);
      }

    //   //return PL_child1_reco_bestFit;
      Double_t DstPx = PL_child1_reco_bestFit.Px();
      Double_t DstPy = PL_child1_reco_bestFit.Py();
      Double_t DstPz = PL_child1_reco_bestFit.Pz();
      Double_t DstE = PL_child1_reco_bestFit.E();
      
      std::cout << "Dst momentum:" << std::endl;
      std::cout << DstPx << ", " << DstPy << ", " << DstPz << ", " << DstE << std::endl;
      
       ROOT::Math::PxPyPzEVector reco_momentum_Dst (DstPx, DstPy, DstPz, DstE);
      
     //set the Dst energy and momentum to returned values
       std::cout << "Magnitude of Dst momentum:" << std::endl;
       std::cout << reco_momentum_Dst.P() << std::endl << std::endl;
     
     //set momentum of Dst 
     Dst->setMomentum( reco_momentum_Dst );
    
     //set Dst momentum covariance matrix to twice the D0 covariance for good measure
     std::cout << "Dst momentum covariance matrix:" << std::endl;
     std::cout << Dst->momCovMatrix() << std::endl << std::endl;

     Dst->setMomCovMatrix(2*d0_cov);

     std::cout << "Dst momentum covariance matrix:" << std::endl;
     std::cout << Dst->momCovMatrix() << std::endl << std::endl;

     //reconstruct the Lambda_b
     Lb->setMomentum( Lc->momentum() + Dst->momentum() + K->momentum() );
     Lb->addToDaughters( Lc );
     Lb->addToDaughters( Dst );
     Lb->addToDaughters( K );
    
     std::cout << "We did it?" << std::endl;
    
     std::cout << "Lc, Dst, K and Lb momentum in order:" << std::endl;
     std::cout << Lc->momentum() << std::endl;
     std::cout << Dst->momentum() << std::endl;
     std::cout << K->momentum() << std::endl;
     std::cout << Lb->momentum() << std::endl;
  
     std::cout << "Measured Masses of Lc, Dst, K and Lb in order:" << std::endl;
     std::cout << Lc->momentum().M() << std::endl;
     std::cout << Dst->momentum().M() << std::endl;
     std::cout << K->momentum().M() << std::endl;
     std::cout << Lb->momentum().M() << std::endl;

     std::cout << "Lb and Dst end vertex:" << std::endl;
     std::cout << Lb->endVertex() << std::endl;
     std::cout << Dst->endVertex() << std::endl;

  return Lb; //true signals success

  
}

typedef LoKi::Hybrid::DictTransform<LoKi::Hybrid::DictIdentityTransform,ConeMethodPartialRecoTransform> PartialReco;
DECLARE_COMPONENT( PartialReco )
///--------------------------------------------------------------------------------------
