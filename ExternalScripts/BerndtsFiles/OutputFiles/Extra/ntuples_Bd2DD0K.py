from Configurables import (DaVinci, DecayTreeTuple, MCDecayTreeTuple, GaudiSequencer)
from DecayTreeTuple import Configuration
###############################################################################

line_name = 'Bd2DD0K_MVA'#'Bd2DD0K_Test_Line'
add_hlt1_tistos = False

seq = GaudiSequencer('seq')
dtt = DecayTreeTuple('CandidatesFrom{0}'.format(line_name))
mc_dtt = MCDecayTreeTuple('MCCandidatesFrom{0}'.format(line_name))

##########

# configure the TisTos tuple tool
if add_hlt1_tistos:
    tistos = dtt.addTupleTool('TupleToolTISTOS')
    tistos.FillL0 = False
    tistos.VerboseHlt1 = True
    tistos.FillHlt2 = False
    tistos.TriggerList = [
        'Hlt1DiMuonHighMassDecision',
        'Hlt1DiMuonHighMassTightDecision',
        'Hlt1DiMuonLowMassDecision',
        'Hlt1DiMuonLowMassTightDecision',
        'Hlt1TrackMVALooseDecision',
        'Hlt1TrackMVATightDecision',
        'Hlt1TrackMVAVLooseDecision',
        'Hlt1TrackMVAVTightDecision',
        'Hlt1TrackMuonMVADecision',
        'Hlt1TwoTrackMVALooseDecision',
        'Hlt1TwoTrackMVATightDecision',
        'Hlt1TwoTrackMVAVLooseDecision',
        'Hlt1TwoTrackMVAVTightDecision'
    ]

mctruth = dtt.addTupleTool('TupleToolMCTruth')
mctruth.ToolList += [
    'MCTupleToolKinematic'
]

###############################################################################

# configure the DecayTreeTuple

dtt.Inputs = ['Phys/{}/Particles'.format(line_name)]
dtt.Decay = '[B0 -> ^(D- -> ^K+ ^pi- ^pi-) ^(D0 -> ^K- ^pi+)]CC'
dtt.addBranches({
    'B0' : '[B0 -> D- D0]CC',
    'B0_Dm' : '[B0 -> ^D- D0]CC',
    'B0_D0' : '[B0 -> D- ^D0]CC'
})

dtt.ToolList += ['TupleToolMCBackgroundInfo',
    'TupleToolPropertime',
    'TupleToolPid',
    'TupleToolGeometry',
    'TupleToolEventInfo',
    'TupleToolTrackInfo',
    'TupleToolPrimaries'
]

####################################

# configure the MCDecayTree tuple
mc_dtt.Decay = '[B0 -> ^(D- -> ^K+ ^pi- ^pi-) ^(D0 -> ^K- ^pi+)]CC'
mc_dtt.addBranches({
    'B0' : '[B0 -> D- D0]CC',
    'B0_Dm' : '[B0 -> ^D- D0]CC',
    'B0_D0' : '[B0 -> D- ^D0]CC'
})

mc_dtt.ToolList = [
    "MCTupleToolHierarchy",         #generated informations about parent and grand-parent
    "LoKi::Hybrid::MCTupleTool",    #to add more variables in the tuple
    "TupleToolEventInfo",
    "MCTupleToolAngles"         #generated decay angles
    ]

#adding more MC variables
kin_tool = mc_dtt.addTupleTool("LoKi::Hybrid::MCTupleTool", "Kinematic")

#adding more MC variables
kin_tool.Variables = {
      # be careful: for some weird reason, many entries of the tree might be filled
      # with unsense values of TRUE_P and TRUE_ETA:
      # don't rely (too much) on them!
      "TRUE_P"   : "MCP",
      "TRUE_ETA" : "MCETA"
    }

mc_dtt.addTupleTool("MCTupleToolKinematic").Verbose = True


####################################
seq.Members += [dtt, mc_dtt]
DaVinci().appendToMainSequence([seq])
DaVinci().InputType = 'MDST'
DaVinci().RootInTES = '/Event/ALLMDST'
DaVinci().DataType = 'Upgrade'
DaVinci().Simulation = True
DaVinci().TupleFile = '{}.root'.format(dtt.getName())

DaVinci().UserAlgorithms = [mc_dtt]

from GaudiConf import IOHelper
IOHelper().inputFiles([
    #'/afs/cern.ch/work/n/nhoyer/public/upgrade-bandwidth-studies/options/Bd2DD0K/wHLT1_Dm_tight_1_5000/000000.ALLMDST.mdst'
    '/afs/cern.ch/work/n/nhoyer/public/upgrade-bandwidth-studies/options/Bd2DD0K/Bd2DD0K_mc/20181212-124510/000000.ALLMDST.mdst'
])
