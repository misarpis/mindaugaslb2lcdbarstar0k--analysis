
# LbLogin -c x86_64-slc6-gcc62-opt; /eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/code_analysis/DaVinci_v43r1/run gaudirun.py mvaLb2LcD0K.py
# 2016 MC: LbLogin -c x86_64-slc6-gcc49-opt; /eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/code_analysis/DaVinci_v41r4p4/run gaudirun.py mvaLb2LcD0K.py
# 2015 MC: LbLogin -c x86_64-slc6-gcc49-opt; /eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/code_analysis/DaVinci_v38r1p6/run gaudirun.py mvaLb2LcD0K.py

################################################################################
"""
    ADDING BDT TO TUPLE

    Example script to run a BDT at DaVinci level over a uDST.
    The exact BDT is used to distinguish:
        D+ -> K- pi+ pi+
    originating from a B. More details available if required.
    We use the example decay:
        [B+ -> ^(D+ -> ^pi+ ^pi+ ^K-) ^(rho(770)0 -> ^pi+ ^pi-)]CC'
    from the B2DPiPiD2HHHCFPIDBeauty2CharmLine.

    Workflow goes as the following:
        - Create selection from stripping using FilterDesktop
        - Create tuple
        - Add branch for the D to the tuple
        - Add Dict2Tuple tool (adds elements of dictionary to tuple)
        - Add TMVAClassifier to Dict2Tuple tool
            - TMVAClassifier is a DictTransform
        - Configure TMVA options
        - Add variables to TMVA, must be named the same as in the .xml
        - Will run over a few files at cern by default

    Tutorial begins at TUTORIAL

    N.B. labX notation for input BDT is used with this LoKi notation of CHILD

"""

__title__ = "mvaDfromB.py"
__author__ = ["Sam Hall", "Sebastian Neubert", "Alessio Piucci"]
__email__ = ["shall@cern.ch", "sneubert@cern.ch", "apiucci@cern.ch"]

#to write a DST output file
WriteDST = False

# job options:
# - "MC" or "Data"
# - "magDown" or "MagUp"
# - year, in string format
# - [optionally, for MC] specify "restrip" if you want to restrip the MC
# - to select the partially-reconstructed channels: "partialD0pi0" or "partialD0g"
#MyOptions = ["MC", "magDown", "2016"]
MyOptions = ["MC", "magDown", "2015", "partialD0g"]
#MyOptions = ["Data", "magDown", "2018"]

# if you want to run locally, over some files placed at CERN EOS
LocalRun = True

#MessageSvc().OutputLevel = "DEBUG"


##############################################################

# set the stripping configuration

stripping_line = "X2LcD0KD02KPiBeauty2CharmLine"

tesLoc = ""

if "Data" in MyOptions:
    tesLoc = "Phys/" + stripping_line + "/Particles"  # don't preceed by '/' for uDST
else :
    # the TES path on MC is slightly different
    tesLoc = "/Event/AllStreams/Phys/" + stripping_line + "/Particles"
    
# stripping version, only used to restrip the MC
stripping_version = "-1"

if "2011" in MyOptions :
    #stripping_version = "21r1"
    stripping_version = "28r1"
elif "2012" in MyOptions :
    #stripping_version = "21"
    stripping_version = "28r1"
elif "2015" in MyOptions :
    #stripping_version = "24r1"
    stripping_version = "28r1"
elif "2016" in MyOptions :
    stripping_version = "28r1"
elif "2017" in MyOptions :
    stripping_version = "29r2"
elif "2018" in MyOptions :
    stripping_version = "34"
      
################################################################################
# Imports
################################################################################
from DecayTreeTuple.Configuration import *

from Configurables import DaVinci
from Configurables import FilterDesktop

from PhysSelPython.Wrappers import Selection
from PhysSelPython.Wrappers import SelectionSequence
from PhysSelPython.Wrappers import DataOnDemand

from Configurables import LoKi__Hybrid__EvtTupleTool

if "MC" in MyOptions:
  from Configurables import MCTupleToolKinematic, MCTupleToolHierarchy, MCDecayTreeTuple

################################################################################
# Config

if "MC" in MyOptions:
  DaVinci().InputType = 'DST'
  #rootInTES = '/Event/'
  #DaVinci(RootInTES=rootInTES, InputType='DST')

if "Data" in MyOptions:
  rootInTES = '/Event/Bhadron'
  DaVinci(RootInTES=rootInTES, InputType='MDST')

################################################################################

################################################################################
# Imports for LoKi functors
from LoKiPhys.decorators import *
from LoKiArrayFunctors.decorators import *
from LoKiProtoParticles.decorators import *
#from LoKiCore.decorators import *  # For debugging in ipython
################################################################################

def labvar(labX, var):
    """Quick function to return formatted lab_variable"""
    return 'lab{}_{}'.format(labX, var)


################################################################################

def get_preambulo():
    """Add shorthands to prambulo to make LoKi functors easier to read"""
    preambulo = [
        'fitVeloChi2 = TINFO(LHCb.Track.FitVeloChi2,-1)',
        'fitVeloNdof = TINFO(LHCb.Track.FitVeloNDoF,-1)',
        'fitTNdof = TINFO(LHCb.Track.FitTNDoF,-1)',
        'fitTChi2 = TINFO(LHCb.Track.FitTChi2,-1)',
    ]
    return preambulo

################################################################################

#Experimental function 
def CHILDIDED(ID,Expr) :
    """wrap expressions expr to apply it to the correct particle"""
    return "switch(CHILD(1,ID)=={0},CHILD(1,{1}),CHILD(2,{1}))".format(ID,Expr)


################################################################################


# the map of child selectors to map the CHILD functor output to the
# variables needed by the BDT
# D+ : K- pi+ pi+
# Ds+ : K+ K- pi+
# D0 : K- pi+

partmap = { 'D' :  [ "'[D+ -> ^K- pi+ pi+]CC'", "'[D+ -> K- ^pi+ pi+]CC'", "'[D+ -> K- pi+ ^pi+]CC'"  ],
            'Ds' : [ "'[D+ -> ^K+ K- pi+]CC'", "'[D+ -> K+ ^K- pi+]CC'", "'[D+ -> K+ K- ^pi+]CC'"  ],
            'Lc' : [ "'[Lambda_c+ -> K- p+ ^pi+]CC'","'[Lambda_c+ -> ^K- p+ pi+]CC'","'[Lambda_c+ -> K- ^p+ pi+]CC'"  ],
            'D0' : [ "'(D0 -> K- ^pi+) || (D0 -> K+ ^pi-)'", "'(D0 -> ^K- pi+) || (D0 -> ^K+ pi-)'", ],
            }


################################################################################

def get_bdt_vars(Dtype='D', labX=2, ndau=3):
    """Return all variables required for the BDT
    Variable names MUST correspond exactly to what is needed by classifier (xml)
    If they are unknown: they are in the xml, and they will be shown in stdout
    """
    #labX = 2  # lab number of parent D
    #ndau = 3  # number of daughters
    bdt_vars = {}
    # Variables for D and daughters; labX_ prefix added later
    vars_parent = {
        'P' : 'P',
        'PT' : 'PT',
        ## 'ENDVERTEX_CHI2' : 'VFASPF(VCHI2)',
        ## 'IPCHI2_OWNPV' : 'MIPCHI2DV(PRIMARY)',
        ## 'FDCHI2_OWNPV' : 'BPVVDCHI2',
    }
    vars_daughters = {
        'P' : 'CHILD(P,{0})',
        'PT' : 'CHILD(PT,{0})',
        'PE' : 'CHILD(E,{0})',
        'PX' : 'CHILD(PX,{0})',
        'PY' : 'CHILD(PY,{0})',
        'PZ' : 'CHILD(PZ,{0})',
        ## 'IPCHI2_OWNPV' : 'CHILD(MIPCHI2DV(PRIMARY),{0})',
        ## # If NDOF > 0 then CHi2/NDOF else -1
        ## 'TRACK_VeloCHI2NDOF' : 'switch(CHILD(fitVeloNdof,{0})>0,CHILD(fitVeloChi2,{0})/CHILD(fitVeloNdof,{0}),-1)',
        ## 'TRACK_TCHI2NDOF' : 'switch(CHILD(fitTNdof,{0})>0,CHILD(fitTChi2,{0})/CHILD(fitTNdof,{0}),-1)',
        ## 'TRACK_MatchCHI2' : 'CHILD(TINFO(LHCb.Track.FitMatchChi2,-1.),{0})',
        ## 'TRACK_GhostProb' : 'CHILD(TRGHOSTPROB,{0})',
        ## 'UsedRichAerogel' : 'switch(CHILDCUT(PPCUT(PP_USEDAEROGEL),{0}),1,0)',
        ## 'UsedRich1Gas' : 'switch(CHILDCUT(PPCUT(PP_USEDRICH1GAS),{0}),1,0)',
        ## 'UsedRich2Gas' : 'switch(CHILDCUT(PPCUT(PP_USEDRICH2GAS),{0}),1,0)',
        ## 'RichAbovePiThres' : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_PI),{0}),1,0)',
        ## 'RichAboveKaThres' : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_K),{0}),1,0)',
        ## 'RichAbovePrThres' : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_P),{0}),1,0)',
        ## 'RichDLLe'  : 'CHILD(PPINFO(LHCb.ProtoParticle.RichDLLe,-1000),{0})',
        ## 'RichDLLmu' : 'CHILD(PPINFO(LHCb.ProtoParticle.RichDLLmu,-1000),{0})',
        ## 'RichDLLk'  : 'CHILD(PPINFO(LHCb.ProtoParticle.RichDLLk,-1000),{0})',
        ## 'RichDLLp'  : 'CHILD(PPINFO(LHCb.ProtoParticle.RichDLLp,-1000),{0})',
        ## 'RichDLLbt' : 'CHILD(PPINFO(LHCb.ProtoParticle.RichDLLbt,-1000),{0})',
        ## 'MuonLLbg'  : 'switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonBkgLL,-10000),{0}),-1000)',
        ## 'MuonLLmu'  : 'switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonMuLL,-10000),{0}),-1000)',
        ## 'isMuon' : 'switch(CHILDCUT(ISMUON,{0}),1,0)',
        ## 'MuonNShared' : 'switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonNShared,0),{0}),-1)',
        ## 'VeloCharge' : 'CHILD(PPINFO(LHCb.ProtoParticle.VeloCharge,-1000),{0})',
    }
    # Add all parent D variables to output
    for var, loki in vars_parent.iteritems():
        bdt_vars.update({labvar(labX, var) : loki})
    # Add all daughter variables to output
    for child, lab in enumerate(range(labX + 1, labX + ndau + 1)):
        for var, loki in vars_daughters.iteritems():
            bdt_vars.update({labvar(lab, var) : loki.format(partmap[Dtype][child])})
    # Print out variables for sanity
    for key in sorted(bdt_vars):
        print '{:<25} : {}'.format(key, bdt_vars[key].replace(',', ', '))
    print 80 * '-'
    return bdt_vars


################################################################################

def get_selection_sequence(name):
    """Get the selection from stripping stream"""
    
    alg = FilterDesktop('SelFilterFor{}B2D'.format(name))

    # apply the PID cut only on data
    if "Data" in MyOptions :
        alg.Code = 'CHILD(PROBNNK,3) > 0.01'
    else :
        alg.Code = 'ALL'
      
    reqSels = [DataOnDemand(Location=tesLoc)]
    sel = Selection('Sel' + name, Algorithm=alg, RequiredSelections=reqSels)
    
    return SelectionSequence('SelSeq' + name, TopSelection=sel)


################################################################################

#DecayTreeTuple
def decay_tree_tuple(name, tuple_input, decay):
    """Create simple DecayTree"""

    from Configurables import TupleToolDecayTreeFitter
    ## from Configurables import TupleToolTrackIsolation 
    ## from Configurables import TupleToolSelResults
    ## from Configurables import TupleToolProtoPData
    ## from Configurables import TupleToolTrackInfo
    from Configurables import TupleToolRecoStats
    from Configurables import TupleToolKinematic
    ## from Configurables import TupleToolGeometry
    ## from Configurables import TupleToolCaloHypo
    ## from Configurables import TupleToolTagging
    from Configurables import TupleToolDecay
    ## from Configurables import TupleToolPid
        
    tpl = DecayTreeTuple('{}DTTuple'.format(name))
    tpl.Inputs = [tuple_input]
    ## tpl.ToolList = [
    ##     'TupleToolKinematic',
    ##     'TupleToolPid',
    ##     'TupleToolANNPID',
    ##     'TupleToolGeometry',
    ##     'TupleToolPrimaries',
    ##     'TupleToolTrackInfo',
    ##     'TupleToolEventInfo',   #Event infos
    ##     'TupleToolAngles',
    ##     "LoKi::Hybrid::EvtTupleTool/LoKiEvent"
    ## ]

    ## tpl.addTool(TupleToolGeometry,name="TupleToolGeometry")
    ## tpl.TupleToolGeometry.Verbose = True
    
    ## tpl.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
    ## tpl.TupleToolTrackInfo.Verbose = True
    
    ## tpl.addTool(TupleToolPid,name="TupleToolPid")
    ## tpl.TupleToolPid.Verbose = True

    #adding more variables to the output ntuple
    LoKiTool = tpl.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
    
    LoKiTool.Variables = {
     "ETA"        : "ETA"
     }
    
    ## #adding some event variables, for the PIDCalib
    ## LoKiEventTuple = LoKi__Hybrid__EvtTupleTool("LoKiEvent")
    ## LoKiEventTuple.Preambulo = [
    ##   "from LoKiTracks.decorators import *",
    ##   "from LoKiCore.functions import *"
    ##   ]
    
    ## LoKiEventTuple.VOID_Variables =  {
    ##   "nTracks"      : "RECSUMMARY( LHCb.RecSummary.nTracks , -9999 )",
    ##   "nPVs"         : "RECSUMMARY( LHCb.RecSummary.nPVs    , -9999)",
    ##   "nSpd"         : "RECSUMMARY( LHCb.RecSummary.nSPDhits, -9999)",  ## multiplicity in the SPD
    ##   }
    
    ## tpl.addTool(LoKiEventTuple)
                                    
    # For dubgging and testing:
    #LoKiTool = tpl.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
    # directly get pt of first D daughters
    #LoKiTool.Variables = {
    #    "LD1_PT" : "CHILD(1,CHILD(PT,1))",
    #    "LD2_PT" : "CHILD(1,CHILD(PT,2))",
    #    "LD3_PT" : "CHILD(1,CHILD(PT,3))",
    #    "LD1_ID" : "CHILD(1,CHILD(ID,1))",
    #    "LD2_ID" : "CHILD(1,CHILD(ID,2))",
    #    "LD_PI_PT" : "CHILD(1,CHILD(PT, 'pi+' == ABSID))", # we have two pi+ so what will happen? --> only first
    #    "LD3_ID" : "CHILD(1,CHILD(ID,3))",
    #    "LDs1_PT" : "CHILD(2,CHILD(PT,1))",
    #    "LDs2_PT" : "CHILD(2,CHILD(PT,2))",
    #    "LDs3_PT" : "CHILD(2,CHILD(PT,3))",
    #    "LDs1_ID" : "CHILD(2,CHILD(ID,1))",
    #    "LDs2_ID" : "CHILD(2,CHILD(ID,2))",
    #    "LDs2_ID_PI" : "CHILD(2,"+CHILDIDED("211","ID")+")",
    #    "LDs2_ID_K" : "CHILD(2,"+CHILDIDED("321","ID")+")",
    #    "LDs2_ID_KFunc" : "CHILD(2,CHILD(ID, 'K+' == ABSID))",
    #    'LDs2_RichAbovePiThres' : "CHILD(2,switch(CHILDCUT('[D+ -> ^K+ K- pi+]CC', PPCUT(PP_RICHTHRES_PI)),1,0))",
    #    "LDs3_ID" : "CHILD(2,CHILD(ID,3))",
    #}

    #list of trigger lines that I want to dump
    ## triglist = [
        
    ##     #L0 lines
    ##     "L0CALODecision",
    ##     "L0DiEM,lowMultDecision",
    ##     "L0DiHadron,lowMultDecision",
    ##     "L0DiMuon,lowMultDecision",
    ##     "L0DiMuonDecision",
    ##     "L0DiMuonNoSPDDecision",
    ##     "L0Electron,lowMultDecision",
    ##     "L0ElectronDecision",
    ##     "L0ElectronHiDecision",
    ##     "L0ElectronNoSPDDecision",
    ##     #"L0GlobalDecision",
    ##     "L0HadronDecision",
    ##     "L0HadronNoSPDDecision",
    ##     "L0HighSumETJetDecision",
    ##     "L0MUON,minbiasDecision",
    ##     "L0Muon,lowMultDecision",
    ##     "L0MuonDecision",
    ##     "L0MuonNoSPDDecision",
    ##     "L0Photon,lowMultDecision",
    ##     "L0PhotonDecision",
    ##     "L0PhotonHiDecision",
    ##     "L0PhotonNoSPDDecision",

    ##     #HLT1 lines
    ##     "Hlt1BeamGasBeam1Decision",
    ##     "Hlt1BeamGasBeam2Decision",
    ##     "Hlt1BeamGasCrossingEnhancedBeam1Decision",
    ##     "Hlt1BeamGasCrossingEnhancedBeam2Decision",
    ##     "Hlt1BeamGasCrossingForcedRecoDecision",
    ##     "Hlt1BeamGasCrossingForcedRecoFullZDecision",
    ##     "Hlt1BeamGasCrossingParasiticDecision",
    ##     "Hlt1BeamGasHighRhoVerticesDecision",
    ##     "Hlt1BeamGasNoBeamBeam1Decision",
    ##     "Hlt1BeamGasNoBeamBeam2Decision",
    ##     "Hlt1CharmCalibrationNoBiasDecision",
    ##     "Hlt1DiMuonHighMassDecision",
    ##     "Hlt1DiMuonLowMassDecision",
    ##     "Hlt1DiProtonDecision",
    ##     "Hlt1DiProtonLowMultDecision",
    ##     "Hlt1ErrorEventDecision",
    ##     #"Hlt1GlobalDecision",
    ##     "Hlt1HighPtJetsSinglePVDecision",
    ##     "Hlt1L0AnyDecision",
    ##     "Hlt1L0AnyNoSPDDecision",
    ##     "Hlt1L0HighSumETJetDecision",
    ##     "Hlt1LumiDecision",
    ##     "Hlt1LumiMidBeamCrossingDecision",
    ##     "Hlt1MBMicroBiasTStationDecision",
    ##     "Hlt1MBMicroBiasVeloDecision",
    ##     "Hlt1MBNoBiasDecision",
    ##     "Hlt1NoPVPassThroughDecision",
    ##     "Hlt1ODINTechnicalDecision",
    ##     "Hlt1SingleElectronNoIPDecision",
    ##     "Hlt1SingleMuonHighPTDecision",
    ##     "Hlt1SingleMuonNoIPDecision",
    ##     "Hlt1Tell1ErrorDecision",
    ##     "Hlt1TrackAllL0Decision",
    ##     "Hlt1TrackAllL0TightDecision",
    ##     "Hlt1TrackForwardPassThroughDecision",
    ##     "Hlt1TrackForwardPassThroughLooseDecision",
    ##     "Hlt1TrackMuonDecision",
    ##     "Hlt1TrackPhotonDecision",
    ##     "Hlt1VeloClosingMicroBiasDecision",
    ##     "Hlt1VertexDisplVertexDecision",
        
    ##     #HLT2 lines
    ##     # Run1 lines
    ##     "Hlt2Topo2BodyBBDTDecision",
    ##     "Hlt2Topo2BodySimpleDecision",
    ##     "Hlt2Topo3BodyBBDTDecision",
    ##     "Hlt2Topo3BodySimpleDecision",
    ##     "Hlt2Topo4BodyBBDTDecision",
    ##     "Hlt2Topo4BodySimpleDecision",
    ##     "Hlt2TopoE2BodyBBDTDecision",
    ##     "Hlt2TopoE3BodyBBDTDecision",
    ##     "Hlt2TopoE4BodyBBDTDecision",
    ##     "Hlt2TopoMu2BodyBBDTDecision",
    ##     "Hlt2TopoMu3BodyBBDTDecision",
    ##     "Hlt2TopoMu4BodyBBDTDecision",
    ##     "Hlt2TopoRad2BodyBBDTDecision",
    ##     "Hlt2TopoRad2plus1BodyBBDTDecision",
    ##     "Hlt2IncPhiDecision",
    ##     "Hlt2IncPhiSidebandsDecision",
        
    ##     # Run 2 lines
    ##     'Hlt2Topo2BodyDecision',
    ##     'Hlt2Topo3BodyDecision',
    ##     'Hlt2Topo4BodyDecision',
    ##     'Hlt2TopoMu2BodyDecision',
    ##     'Hlt2TopoMu3BodyDecision',
    ##     'Hlt2TopoMu4BodyDecision',
    ##     'Hlt2TopoE2BodyDecision',
    ##     'Hlt2TopoE3BodyDecision',
    ##     'Hlt2TopoE4BodyDecision',
    ##     'Hlt2TopoMuMu2BodyDecision',
    ##     'Hlt2TopoMuMu3BodyDecision',
    ##     'Hlt2TopoMuMu4BodyDecision',
    ##     'Hlt2TopoEE2BodyDecision',
    ##     'Hlt2TopoEE3BodyDecision',
    ##     'Hlt2TopoEE4BodyDecision',
    ##     'Hlt2TopoMuE2BodyDecision',
    ##     'Hlt2TopoMuE3BodyDecision',
    ##     'Hlt2TopoMuE4BodyDecision',
    ##     'Hlt2TopoMuMuDDDecision'
    ##     ]

    #adding MC-truth, only for MC samples
    ## if "MC" in MyOptions:
  
      ## MCTruth = tpl.addTupleTool("TupleToolMCTruth")
    
      ## MCTruth.ToolList += [
      ##                    'MCTupleToolKinematic',    #kinematic generated variables
      ##                    ## 'MCTupleToolHierarchy',    #generated informations about parent and grand-parent
      ##                    ## "MCTupleToolAngles",       #generated decay angles
      ##                     "LoKi::Hybrid::MCTupleTool/LokiTool"   #to add more generated variables in the tuple
      ##                    ]
      
      ## ## tpl.addTupleTool("TupleToolMCBackgroundInfo")
      
      ## MCTruth.addTool(MCTupleToolKinematic())
      ## MCTruth.MCTupleToolKinematic.Verbose = True
      
      ## from Configurables import LoKi__Hybrid__MCTupleTool
      ## LokiTool = LoKi__Hybrid__MCTupleTool("LokiTool")
      
      ## #adding more generated variables
      ## LokiTool.Variables = {
      ##   "TRUE_P"   : "MCP",
      ##   "TRUE_ETA" : "MCETA",
      ##   "TRUE_M"   : "MCM"
      ## }
      
      ## MCTruth.addTool(LokiTool)

    #adding MC-truth

    
    #add TIS/TOS trigger variables
    from Configurables import TupleToolTISTOS, TupleToolTrigger, TriggerTisTos

    ## #TISTOSTool
    ## TISTOSTool = TupleToolTISTOS('TISTOSTool')
    ## TISTOSTool.VerboseL0   = True
    ## TISTOSTool.VerboseHlt1 = False
    ## TISTOSTool.VerboseHlt2 = True
    ## TISTOSTool.TriggerList = triglist[:]
    ## TISTOSTool.TIS = True
    ## TISTOSTool.TOS = True
    ## TISTOSTool.TUS = True
    ## TISTOSTool.TPS = True
    ## TISTOSTool.addTool( TriggerTisTos, name = "TriggerTisTos")
    
    ## tpl.addTool(TISTOSTool, name = "TISTOSTool")
    ## tpl.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]
    
    ## #TriggerTool
    ## TriggerTool = tpl.addTupleTool("TupleToolTrigger/TriggerTool")
    ## TriggerTool.VerboseL0   = True
    ## TriggerTool.VerboseHlt1 = False
    ## TriggerTool.VerboseHlt2 = True
    ## TriggerTool.TriggerList = triglist[:]
    

    # Output distributions for pions do not match unless labX == True
    tpl.UseLabXSyntax = True  # True
    tpl.RevertToPositiveID = False
    tpl.Decay = decay
    return tpl

################################################################################

#MCDecayTreeTuple
## def decay_tree_tuple_Full_MC(name, decay):
  
##   """Create an MCDecayTree with full MC informations"""
  
##   mc_tpl = MCDecayTreeTuple("MCDecayTreeTuple")
  
##   mc_tpl.ToolList = [
##     "MCTupleToolHierarchy",         #generated informations about parent and grand-parent
##     "LoKi::Hybrid::MCTupleTool",    #to add more variables in the tuple
##     "TupleToolEventInfo",
##     "MCTupleToolAngles"         #generated decay angles
##     ]
  
##   #adding more MC variables
##   kin_tool = mc_tpl.addTupleTool("LoKi::Hybrid::MCTupleTool", "Kinematic")
                     
##   #adding more MC variables
##   kin_tool.Variables = {
##     "TRUE_P"   : "MCP",
##     "TRUE_ETA" : "MCETA"
##     }
  
##   mc_tpl.addTupleTool("MCTupleToolKinematic").Verbose = True

##   #add TIS/TOS trigger variables
##   from Configurables import TupleToolTrigger, TriggerTisTos
  
##   #list of trigger lines on which I'm interested
##   triglist = [
##     #L0 lines
##     "L0CALODecision",
##     "L0DiEM,lowMultDecision",
##     "L0DiHadron,lowMultDecision",
##     "L0DiMuon,lowMultDecision",
##     "L0DiMuonDecision",
##     "L0DiMuonNoSPDDecision",
##     "L0Electron,lowMultDecision",
##     "L0ElectronDecision",
##     "L0ElectronHiDecision",
##     "L0ElectronNoSPDDecision",
##     #"L0GlobalDecision",
##     "L0HadronDecision",
##     "L0HadronNoSPDDecision",
##     "L0HighSumETJetDecision",
##     "L0MUON,minbiasDecision",
##     "L0Muon,lowMultDecision",
##     "L0MuonDecision",
##     "L0MuonNoSPDDecision",
##     "L0Photon,lowMultDecision",
##     "L0PhotonDecision",
##     "L0PhotonHiDecision",
##     "L0PhotonNoSPDDecision",
    
##     #HLT1 lines
##     "Hlt1BeamGasBeam1Decision",
##     "Hlt1BeamGasBeam2Decision",
##     "Hlt1BeamGasCrossingEnhancedBeam1Decision",
##     "Hlt1BeamGasCrossingEnhancedBeam2Decision",
##     "Hlt1BeamGasCrossingForcedRecoDecision",
##     "Hlt1BeamGasCrossingForcedRecoFullZDecision",
##     "Hlt1BeamGasCrossingParasiticDecision",
##     "Hlt1BeamGasHighRhoVerticesDecision",
##     "Hlt1BeamGasNoBeamBeam1Decision",
##     "Hlt1BeamGasNoBeamBeam2Decision",
##     "Hlt1CharmCalibrationNoBiasDecision",
##     "Hlt1DiMuonHighMassDecision",
##     "Hlt1DiMuonLowMassDecision",
##     "Hlt1DiProtonDecision",
##     "Hlt1DiProtonLowMultDecision",
##     "Hlt1ErrorEventDecision",
##     #"Hlt1GlobalDecision",
##     "Hlt1HighPtJetsSinglePVDecision",
##     "Hlt1L0AnyDecision",
##     "Hlt1L0AnyNoSPDDecision",
##     "Hlt1L0HighSumETJetDecision",
##     "Hlt1LumiDecision",
##     "Hlt1LumiMidBeamCrossingDecision",
##     "Hlt1MBMicroBiasTStationDecision",
##     "Hlt1MBMicroBiasVeloDecision",
##     "Hlt1MBNoBiasDecision",
##     "Hlt1NoPVPassThroughDecision",
##     "Hlt1ODINTechnicalDecision",
##     "Hlt1SingleElectronNoIPDecision",
##     "Hlt1SingleMuonHighPTDecision",
##     "Hlt1SingleMuonNoIPDecision",
##     "Hlt1Tell1ErrorDecision",
##     "Hlt1TrackAllL0Decision",
##     "Hlt1TrackAllL0TightDecision",
##     "Hlt1TrackForwardPassThroughDecision",
##     "Hlt1TrackForwardPassThroughLooseDecision",
##     "Hlt1TrackMuonDecision",
##     "Hlt1TrackPhotonDecision",
##     "Hlt1VeloClosingMicroBiasDecision",
##     "Hlt1VertexDisplVertexDecision",
    
##     #HLT2 lines
##     "Hlt2Topo2BodyBBDTDecision",
##     "Hlt2Topo2BodySimpleDecision",
##     "Hlt2Topo3BodyBBDTDecision",
##     "Hlt2Topo3BodySimpleDecision",
##     "Hlt2Topo4BodyBBDTDecision",
##     "Hlt2Topo4BodySimpleDecision",
##     "Hlt2TopoE2BodyBBDTDecision",
##     "Hlt2TopoE3BodyBBDTDecision",
##     "Hlt2TopoE4BodyBBDTDecision",
##     "Hlt2TopoMu2BodyBBDTDecision",
##     "Hlt2TopoMu3BodyBBDTDecision",
##     "Hlt2TopoMu4BodyBBDTDecision",
##     "Hlt2TopoRad2BodyBBDTDecision",
##     "Hlt2TopoRad2plus1BodyBBDTDecision",
##     "Hlt2IncPhiDecision",
##     "Hlt2IncPhiSidebandsDecision"
##     ]
  
##   TriggerTool = mc_tpl.addTupleTool("TupleToolTrigger/TriggerTool")
##   TriggerTool.VerboseL0   = True
##   TriggerTool.VerboseHlt1 = True
##   TriggerTool.VerboseHlt2 = True
##   TriggerTool.TriggerList = triglist[:]
  
##   # Output distributions for pions do not match unless labX == True
##   mc_tpl.UseLabXSyntax = True
##   mc_tpl.RevertToPositiveID = False
##   mc_tpl.Decay = decay
  
##   return mc_tpl

################################################################################
# Make selection
################################################################################
name = 'Lb2LcD0K'
decay = '(B0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(D0 -> ^K+ ^pi-) ^K-) || (B0 -> ^(Lambda_c~- -> ^p~- ^K+ ^pi-) ^(D0 -> ^K- ^pi+) ^K+)'

#I use this Loki descriptor only for the MCDecayTuple
decay_MC = '(B0 -> ^(Lambda_c+ --> ^p+ ^K- ^pi+) ^(D~0 -> ^K+ ^pi-) ^K-) || (B0 -> ^(Lambda_c~- --> ^p~- ^K+ ^pi-) ^(D0 -> ^K- ^pi+) ^K+)'

# check if we are really running on partial reco channels
if 'partialD0g' in MyOptions :
  decay_MC = '(Lambda_b0 -> ^(Lambda_c+ --> ^p+ ^K- ^pi+) ^(D*(2007)~0 --> ^(D~0 -> ^K+ ^pi-) ^gamma) ^K-) || (Lambda_b~0 -> ^(Lambda_c~- --> ^p~- ^K+ ^pi-) ^(D*(2007)0 --> ^(D0 -> ^K- ^pi+) ^gamma) ^K+)'
elif 'partialD0pi0' in MyOptions :
  decay_MC = '(Lambda_b0 -> ^(Lambda_c+ --> ^p+ ^K- ^pi+) ^(D*(2007)~0 --> ^(D~0 -> ^K+ ^pi-) ^pi0) ^K-) || (Lambda_b~0 -> ^(Lambda_c~- --> ^p~- ^K+ ^pi-) ^(D*(2007)0 --> ^(D0 -> ^K- ^pi+) ^pi0) ^K+)'
    
seq = GaudiSequencer('My{}DTTupleSeq'.format(name))

# selection sequence when you don't want to restrip
if ("Data" in MyOptions) or (("MC" in MyOptions) and not ("restrip" in MyOptions)) :
  sel = get_selection_sequence(name)
  seq.Members += [sel.sequence()]
  tuple_input = sel.outputLocation()
else :
  # where the particles are placed for restripped MC
  tuple_input = "/Event/Phys/" + stripping_line + "/Particles"

# make the tuple
tpl = decay_tree_tuple(name, tuple_input, decay)

# tuple with informations from MC generated signals
##if "MC" in MyOptions:
##  mctpl = decay_tree_tuple_Full_MC(name, decay_MC)

################################################################################
#
################################################################################
# Add branch of the decay where BDT is added
#  - in this case adding to (D+ -> K- pi+ pi+)

tpl.addBranches({'Lc' : '[Beauty -> ^Lambda_c+ Meson Meson]CC'})
LoKi_Lc = tpl.Lc.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Lc')

tpl.addBranches({'D0' : '[Beauty -> Baryon ^Xc Meson]CC'})
LoKi_D0 = tpl.D0.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_D0')
tpl.addBranches({'gamma' : '[Beauty -> Baryon (D*(2007)~0 -> D~0 ^gamma) Meson]CC'})
LoKi_gamma = tpl.gamma.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_gamma')

## if "MC" in MyOptions:
##   if "partialD0g" in MyOptions:
##     mctpl.addBranches({"Lb" : "(Lambda_b0 -> Lambda_c+ Charm K-) || (Lambda_b~0 -> Lambda_c~- Xc K+)"})
##     mctpl.addBranches({'Lc' : '[Beauty -> ^Lambda_c+ Meson Meson]CC'})
##     mctpl.addBranches({'Dst' : '[Beauty -> Baryon ^Xc Meson]CC'})
##     mctpl.addBranches({'D0' : '[Beauty -> Baryon (D*(2007)~0 -> ^D~0 gamma) Meson]CC'})
##   elif "partialD0pi0" in MyOptions:
##     mctpl.addBranches({"Lb" : "(Lambda_b0 -> Lambda_c+ Charm K-) || (Lambda_b~0 -> Lambda_c~- Xc K+)"})
##     mctpl.addBranches({'Lc' : '[Beauty -> ^Lambda_c+ Meson Meson]CC'})
##     mctpl.addBranches({'Dst' : '[Beauty -> Baryon ^Xc Meson]CC'})
##     mctpl.addBranches({'D0' : '[Beauty -> Baryon (D*(2007)~0 -> ^D~0 pi0) Meson]CC'})
##   else :  
##     mctpl.addBranches({"lab0" : "(Lambda_b0 -> Lambda_c+ D0 K-) || (Lambda_b~0 -> Lambda_c~- D0 K+)"})
##     mctpl.addBranches({'Lc' : '[Beauty -> ^Lambda_c+ Meson Meson]CC'})
##     mctpl.addBranches({'D0' : '[Beauty -> Baryon ^Xc Meson]CC'})
 
# THIS IS THE LINE:
#  pass the banch,, xml, variables, name, and whether you want all variables
#  in the nTuple (default to False), add preamulo if you wish

#BDT running only over Data
#if "Data" in MyOptions:
#  from MVADictHelpers import *
#  
#  addTMVAclassifierTuple(tpl.Lc, 'TMVAnalysis_Lc_BDT_NC.weights.xml', get_bdt_vars('Lc',2, 3),
#                         Name='BDTLc', Keep=False, Preambulo=get_preambulo())
#  addTMVAclassifierTuple(tpl.D0, 'TMVAnalysis_D0_BDT_NC.weights.xml', get_bdt_vars('D0',2, 2),
#                         Name='BDTD0', Keep=False, Preambulo=get_preambulo())


################################################################################
# Add decay tree fitter
from Configurables import TupleToolDecayTreeFitter,TupleToolDecay
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
#from Configurables import LoKi__Hybrid__DictTransform_DictIdentityTransform__CandidateIdentityTransform_ #as a test
#try:
#from Configurables import LoKi__Hybrid__DictTransform_DictIdentityTransform_ConeMethodPartialRecoTransform_
from Configurables import LoKi__Hybrid__DictTransform_LoKi__Hybrid__DictIdentityTransform_ConeMethodPartialRecoTransform_
#    print "Import of ConeMethodPartialRecoTransform successful"
#except:
#    print "Import of ConeMethodPartialRecoTransform failed"
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
tpl.addBranches({"Lb" : "(B0 -> Lambda_c+ D0 K-) || (B0 -> Lambda_c~- D0 K+)"})
#-- 
#tpl.Lb.ToolList =  ["TupleToolDecayTreeFitter/Cons","TupleToolDecayTreeFitter/ConsLb"] # fit with mass constraint
#tpl.Lb.addTool(TupleToolDecayTreeFitter("Cons"))
#tpl.Lb.Cons.constrainToOriginVertex = True
#tpl.Lb.Cons.Verbose = True
#tpl.Lb.Cons.daughtersToConstrain = [ "Lambda_c+","D0" ]

tpl.Lb.ToolList =  ["TupleToolDecayTreeFitter/ConsLb"] # fit with mass constraint

##FIRST DTF (Lc, D0 constrained) -> use DFTDict
# Start by adding the Dict2Tuple to the Lb branch - this will write the values we are going to retrieve into the ntuple
DictTuple = tpl.Lb.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")

if "partialD0g" in MyOptions:
    #try:
    PartialReco = DictTuple.addTool(LoKi__Hybrid__DictTransform_LoKi__Hybrid__DictIdentityTransform_ConeMethodPartialRecoTransform_, "PartialReco")
    DictTuple.Source = "LoKi::Hybrid::DictTransform<LoKi::Hybrid::DictIdentityTransform, ConeMethodPartialRecoTransform>/PartialReco"
    #    print "Addition of PartialReco successful"
    #except:
    #    print "Addition of PartialReco failed"

  

# We need a DecayTreeFitter. DTFDict will provide the fitter and the connection to the tool chain
# we add it as a source of data to the Dict2Tuple
DictTuple.PartialReco.addTool(DTFDict,"Cons")
DictTuple.PartialReco.Source = "LoKi::Hybrid::DTFDict/Cons"
DictTuple.NumVar = 100     # reserve a suitable size for the dictionaire

# configure the DecayTreeFitter in the usual way
DictTuple.PartialReco.Cons.constrainToOriginVertex = True
DictTuple.PartialReco.Cons.daughtersToConstrain=["Lambda_b0","Lambda_c+","D~0"]#,"D*(2007)~0"]

# Add LoKiFunctors to the tool chain, just as we did to the Hybrid::TupleTool above
# these functors will be applied to the refitted(!) decay tree
# they act as a source to the DTFDict
DictTuple.PartialReco.Cons.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.PartialReco.Cons.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.PartialReco.Cons.dict.Variables = {
    "Cons_M"                   : "M",
    #"Cons_M2Err2"              : "M2ERR2",
    "Cons_P"                   : "P",
    #"Cons_PErr2"               : "PERR2",
    "Cons_PT"                  : "PT",
    #"Cons_PTErr2"              : "PTERR2",
    "Cons_ETA"                 : "ETA",
    "Cons_PHI"                 : "PHI",
    "Cons_PE"                  : "E",
    "Cons_PX"                  : "PX",
    "Cons_PY"                  : "PY",
    "Cons_PZ"                  : "PZ",
    #"Cons_Cov_PXPX"            : "PCOV2(0,0)",
    #"Cons_Cov_PYPY"            : "PCOV2(1,1)",
    #"Cons_Cov_PZPZ"            : "PCOV2(2,2)",
    #"Cons_Cov_PEPE"            : "PCOV2(3,3)",
    #"Cons_Cov_PXPY"            : "PCOV2(0,1)",
    #"Cons_Cov_PYPZ"            : "PCOV2(1,2)",
    #"Cons_Cov_PZPE"            : "PCOV2(2,3)",
    #"Cons_Cov_PXPZ"            : "PCOV2(0,2)",
    #"Cons_Cov_PYPE"            : "PCOV2(1,3)",
    #"Cons_Cov_PXPE"            : "PCOV2(0,3)",
    #"Cons_BPV_X"               : "BPV(VX)",
    #"Cons_BPV_Y"               : "BPV(VY)",
    #"Cons_BPV_Z"               : "BPV(VZ)",
    #"Cons_ENDVERTEX_X"         : "VFASPF(VX)",
    #"Cons_ENDVERTEX_Y"         : "VFASPF(VY)",
    #"Cons_ENDVERTEX_Z"         : "VFASPF(VZ)",
    #"Cons_Cov_ENDVERTEX_XX"    : "PCOV2(4,4)",#hope this is the one set by the vertex fitter...
    #"Cons_Cov_ENDVERTEX_YY"    : "PCOV2(5,5)",
    #"Cons_Cov_ENDVERTEX_ZZ"    : "PCOV2(6,6)",
    #"Cons_Cov_ENDVERTEX_XY"    : "PCOV2(4,5)",
    #"Cons_Cov_ENDVERTEX_YZ"    : "PCOV2(5,6)",
    #"Cons_Cov_ENDVERTEX_XZ"    : "PCOV2(4,6)",
    #"Cons_DIRA"                : "DIRA", #have to get diras, docas etc. by hand later
    #"Cons_BPVIP"               : "BPVIP()",
    #"Cons_BPVIPCHI2"           : "BPVIPCHI2()",
    #"Cons_BPVLTIME"            : "BPVLTIME()",
    #"Cons_BPVPATHDIST"         : "BPVPATHDIST()",#need to calculate this by hand for Lc and D0 later
    #"Cons_BPVPDS"              : "BPVPDS()",
    
    #"Cons_Lc_M"                : "CHILD(M,1)",
    #"Cons_Lc_M2ERR2"           : "CHILD(M2ERR2,1)",
    #"Cons_Lc_P"                : "CHILD(P,1)",
    #"Cons_Lc_PErr2"            : "CHILD(PERR2,1)",
    #"Cons_Lc_PT"               : "CHILD(PT,1)",
    #"Cons_Lc_PTErr2"           : "CHILD(PTERR2,1)",
    #"Cons_Lc_ETA"              : "CHILD(ETA,1)",
    #"Cons_Lc_PHI"              : "CHILD(PHI,1)",
    #"Cons_Lc_PE"               : "CHILD(E,1)",
    #"Cons_Lc_PX"               : "CHILD(PX,1)",
    #"Cons_Lc_PY"               : "CHILD(PY,1)",
    #"Cons_Lc_PZ"               : "CHILD(PZ,1)",
    #"Cons_Lc_Cov_PXPX"         : "CHILD(PCOV2(0,0),1)",
    #"Cons_Lc_Cov_PYPY"         : "CHILD(PCOV2(1,1),1)",
    #"Cons_Lc_Cov_PZPZ"         : "CHILD(PCOV2(2,2),1)",
    #"Cons_Lc_Cov_PEPE"         : "CHILD(PCOV2(3,3),1)",
    #"Cons_Lc_Cov_PXPY"         : "CHILD(PCOV2(0,1),1)",
    #"Cons_Lc_Cov_PYPZ"         : "CHILD(PCOV2(1,2),1)",
    #"Cons_Lc_Cov_PZPE"         : "CHILD(PCOV2(2,3),1)",
    #"Cons_Lc_Cov_PXPZ"         : "CHILD(PCOV2(0,2),1)",
    #"Cons_Lc_Cov_PYPE"         : "CHILD(PCOV2(1,3),1)",
    #"Cons_Lc_Cov_PXPE"         : "CHILD(PCOV2(0,3),1)",
    #"Cons_Lc_QPT"              : "QPT(1)",
    #"Cons_Lc_ENDVERTEX_X"      : "CHILD(VFASPF(VX),1)",
    #"Cons_Lc_ENDVERTEX_Y"      : "CHILD(VFASPF(VY),1)",
    #"Cons_Lc_ENDVERTEX_Z"      : "CHILD(VFASPF(VZ),1)",
    #"Cons_Lc_Cov_ENDVERTEX_XX" : "CHILD(PCOV2(4,4),1)",
    #"Cons_Lc_Cov_ENDVERTEX_YY" : "CHILD(PCOV2(5,5),1)",
    #"Cons_Lc_Cov_ENDVERTEX_ZZ" : "CHILD(PCOV2(6,6),1)",
    #"Cons_Lc_Cov_ENDVERTEX_XY" : "CHILD(PCOV2(4,5),1)",
    #"Cons_Lc_Cov_ENDVERTEX_YZ" : "CHILD(PCOV2(5,6),1)",
    #"Cons_Lc_Cov_ENDVERTEX_XZ" : "CHILD(PCOV2(4,6),1)",
    #"Cons_Lc_BPVIP"            : "CHILD(BPVIP(),1)",
    #"Cons_Lc_BPVIPCHI2"        : "CHILD(BPVIPCHI2(),1)",
    #"Cons_Lc_DTFctau"          : "DTF_CTAU(1,True)",
    #"Cons_Lc_DTFctauSign"      : "DTF_CTAUSIGNIFICANCE(1,True)",
    
    #"Cons_D0_M"                : "CHILD(M,2)",
    #"Cons_D0_M2ERR2"           : "CHILD(M2ERR2,2)",
    #"Cons_D0_P"                : "CHILD(P,2)",
    #"Cons_D0_PErr2"            : "CHILD(PERR2,2)",
    #"Cons_D0_PT"               : "CHILD(PT,2)",
    #"Cons_D0_PTErr2"           : "CHILD(PTERR2,2)",
    #"Cons_D0_ETA"              : "CHILD(ETA,2)",
    #"Cons_D0_PHI"              : "CHILD(PHI,2)",
    #"Cons_D0_PE"               : "CHILD(E,2)",
    #"Cons_D0_PX"               : "CHILD(PX,2)",
    #"Cons_D0_PY"               : "CHILD(PY,2)",
    #"Cons_D0_PZ"               : "CHILD(PZ,2)",
    #"Cons_D0_Cov_PXPX"         : "CHILD(PCOV2(0,0),2)",
    #"Cons_D0_Cov_PYPY"         : "CHILD(PCOV2(1,1),2)",
    #"Cons_D0_Cov_PZPZ"         : "CHILD(PCOV2(2,2),2)",
    #"Cons_D0_Cov_PEPE"         : "CHILD(PCOV2(3,3),2)",
    #"Cons_D0_Cov_PXPY"         : "CHILD(PCOV2(0,1),2)",
    #"Cons_D0_Cov_PYPZ"         : "CHILD(PCOV2(1,2),2)",
    #"Cons_D0_Cov_PZPE"         : "CHILD(PCOV2(2,3),2)",
    #"Cons_D0_Cov_PXPZ"         : "CHILD(PCOV2(0,2),2)",
    #"Cons_D0_Cov_PYPE"         : "CHILD(PCOV2(1,3),2)",
    #"Cons_D0_Cov_PXPE"         : "CHILD(PCOV2(0,3),2)",
    #"Cons_D0_QPT"              : "QPT(2)",
    #"Cons_D0_ALPHA"            : "CHILD(ARMENTEROS,2)",
    #"Cons_D0_ENDVERTEX_X"      : "CHILD(VFASPF(VX),2)",
    #"Cons_D0_ENDVERTEX_Y"      : "CHILD(VFASPF(VY),2)",
    #"Cons_D0_ENDVERTEX_Z"      : "CHILD(VFASPF(VZ),2)",
    #"Cons_D0_Cov_ENDVERTEX_XX" : "CHILD(PCOV2(4,4),2)",
    #"Cons_D0_Cov_ENDVERTEX_YY" : "CHILD(PCOV2(5,5),2)",
    #"Cons_D0_Cov_ENDVERTEX_ZZ" : "CHILD(PCOV2(6,6),2)",
    #"Cons_D0_Cov_ENDVERTEX_XY" : "CHILD(PCOV2(4,5),2)",
    #"Cons_D0_Cov_ENDVERTEX_YZ" : "CHILD(PCOV2(5,6),2)",
    #"Cons_D0_Cov_ENDVERTEX_XZ" : "CHILD(PCOV2(4,6),2)",
    #"Cons_D0_BPVIP"            : "CHILD(BPVIP(),2)",
    #"Cons_D0_BPVIPCHI2"        : "CHILD(BPVIPCHI2(),2)",
    #"Cons_D0_DTFctau"          : "DTF_CTAU(2,True)",
    #"Cons_D0_DTFctauSign"      : "DTF_CTAUSIGNIFICANCE(2,True)",

    #"Cons_lab8_P"                : "CHILD(P,3)",
    #"Cons_lab8_PErr2"            : "CHILD(PERR2,3)",
    #"Cons_lab8_PT"               : "CHILD(PT,3)",
    #"Cons_lab8_PTErr2"           : "CHILD(PTERR2,3)",
    #"Cons_lab8_ETA"              : "CHILD(ETA,3)",
    #"Cons_lab8_PHI"              : "CHILD(PHI,3)",
    #"Cons_lab8_PE"               : "CHILD(E,3)",
    #"Cons_lab8_PX"               : "CHILD(PX,3)",
    #"Cons_lab8_PY"               : "CHILD(PY,3)",
    #"Cons_lab8_PZ"               : "CHILD(PZ,3)",
    #"Cons_lab8_Cov_PXPX"         : "CHILD(PCOV2(0,0),3)",
    #"Cons_lab8_Cov_PYPY"         : "CHILD(PCOV2(1,1),3)",
    #"Cons_lab8_Cov_PZPZ"         : "CHILD(PCOV2(2,2),3)",
    #"Cons_lab8_Cov_PEPE"         : "CHILD(PCOV2(3,3),3)",
    #"Cons_lab8_Cov_PXPY"         : "CHILD(PCOV2(0,1),3)",
    #"Cons_lab8_Cov_PYPZ"         : "CHILD(PCOV2(1,2),3)",
    #"Cons_lab8_Cov_PZPE"         : "CHILD(PCOV2(2,3),3)",
    #"Cons_lab8_Cov_PXPZ"         : "CHILD(PCOV2(0,2),3)",
    #"Cons_lab8_Cov_PYPE"         : "CHILD(PCOV2(1,3),3)",
    #"Cons_lab8_Cov_PXPE"         : "CHILD(PCOV2(0,3),3)",
    #"Cons_lab8_QPT"              : "QPT(3)",
    #"Cons_lab8_BPVIP"            : "CHILD(BPVIP(),3)",
    #"Cons_lab8_BPVIPCHI2"        : "CHILD(BPVIPCHI2(),3)",

    #"Cons_Lc_MIPCHI2DV_PRIMARY"   : "CHILD(MIPCHI2DV(PRIMARY), 1)",
    #"Cons_D0_MIPCHI2DV_PRIMARY"   : "CHILD(MIPCHI2DV(PRIMARY), 2)",
    #"Cons_lab8_MIPCHI2DV_PRIMARY" : "CHILD(MIPCHI2DV(PRIMARY), 3)",
    
    #"Cons_gamma_P"                : "CHILD(CHILD(P,2),2)",
    
}


##SECOND DTF (Lb, Lc, D0 constrained)

## # Add second decay tree fitter used for Dalitz and angular variables
## tpl.Lb.addTool(TupleToolDecayTreeFitter("ConsLb"))

## tpl.Lb.ConsLb.constrainToOriginVertex = True
## tpl.Lb.ConsLb.Verbose = True
## tpl.Lb.ConsLb.UpdateDaughters = True

## tpl.Lb.ConsLb.Substitutions = { "B0 -> Lambda_c+ D0 K-" : "Lambda_b0",
##                                 "B0 -> Lambda_c~- D0 K+" : "Lambda_b~0" }
## tpl.Lb.ConsLb.daughtersToConstrain = [ "Lambda_b0","Lambda_c+","D0" ]

################################################################################

# build the sequence to re strip the MC, only if required

## if ("MC" in MyOptions) and ("restrip" in MyOptions) :
##   from Configurables import EventNodeKiller, ProcStatusCheck
##   event_node_killer = EventNodeKiller('StripKiller')
##   event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

##   from StrippingConf.Configuration import StrippingConf, StrippingStream
##   from StrippingSettings.Utils import strippingConfiguration
##   from StrippingArchive.Utils import buildStreams
##   from StrippingArchive import strippingArchive

##   strip = 'stripping' + stripping_version
##   streams = buildStreams(stripping = strippingConfiguration(strip),
##                          archive = strippingArchive(strip))

##   custom_stream = StrippingStream('CustomStream')
##   custom_line = 'Stripping' + stripping_line
  
##   #print "strippingConfiguration(strip) = ", strippingConfiguration(strip)
##   #print "strippingArchive(strip) = ", strippingArchive(strip)
  
##   for stream in streams:
##     for line in stream.lines:

##       #print "line.name() = ", line.name()
      
##       if line.name() == custom_line:
##         custom_stream.appendLines([line])

##   filterBadEvents = ProcStatusCheck()

##   sc = StrippingConf(Streams=[custom_stream],
##                      MaxCandidates=2000,
##                      AcceptBadEvents=False,
##                      BadEventSelection=filterBadEvents)

##   seq.Members += [event_node_killer, sc.sequence()]

################################################################################

seq.Members += [tpl]

#add the MCDecayTreeTuple
##if "MC" in MyOptions:
##  seq.Members += [mctpl]
  
DaVinci().appendToMainSequence([seq])

################################################################################

# set of detector conditions for MC
if "MC" in MyOptions:
  DaVinci().Simulation = True
  DaVinci().Lumi = False

  if "2016" in MyOptions:
    DaVinci().DataType = '2016'

    DaVinci().DDDBtag = 'dddb-20170721-3'
    
    if "magDown" in MyOptions:
      DaVinci().CondDBtag = 'sim-20170721-2-vc-md100'
      
    if "magUp" in MyOptions:
      DaVinci().CondDBtag = 'sim-20170721-2-vc-mu100'
                        
    #May 2017 samples
    #DaVinci().DDDBtag = 'dddb-20150724'
    #
    #if "magDown" in MyOptions:
    #  DaVinci().CondDBtag = 'sim-20161124-2-vc-md100'
    #
    #if "magUp" in MyOptions:
    #  DaVinci().CondDBtag = 'sim-20161124-2-vc-mu100'

  if "2015" in MyOptions:
    DaVinci().DataType = '2015'
    
    DaVinci().DDDBtag = 'dddb-20170721-3'
    
    if "magDown" in MyOptions:
      DaVinci().CondDBtag = 'sim-20161124-vc-md100'
      
    if "magUp" in MyOptions:
      DaVinci().CondDBtag = 'sim-20161124-vc-mu100'
                              
  if "2012" in MyOptions:
    DaVinci().DataType = '2012'

    DaVinci().DDDBtag = 'dddb-20150928'
    
    if "magDown" in MyOptions:

      DaVinci().CondDBtag = 'sim-20160321-2-vc-md100'
      
      #old MC sample
      #DaVinci().DDDBtag = 'dddb-20120831'
      #DaVinci().CondDBtag = 'sim-20121025-vc-md100'

      if "partialD0g" in MyOptions or "partialD0pi0" in MyOptions:
        DaVinci().DDDBtag = 'dddb-20130929-1' 
        DaVinci().CondDBtag = 'sim-20141210-1-vc-md100'
    
    if "magUp" in MyOptions:

      DaVinci().CondDBtag = 'sim-20160321-2-vc-mu100'
      
      #old MC sample
      #DaVinci().DDDBtag = 'dddb-20120831'
      #DaVinci().CondDBtag = 'sim-20121025-vc-mu100'

      if "partialD0g" in MyOptions or "partialD0pi0" in MyOptions:
        DaVinci().DDDBtag = 'dddb-20130929-1' 
        DaVinci().CondDBtag = 'sim-20141210-1-vc-mu100'


  if "2011" in MyOptions:
    DaVinci().DataType = '2011'

    DaVinci().DDDBtag = 'dddb-20160318-1'
     
    if "magDown" in MyOptions:
      DaVinci().CondDBtag = 'sim-20160614-1-vc-md100'
      
    if "magUp" in MyOptions:
      DaVinci().CondDBtag = 'sim-20160614-1-vc-mu100'

    #old MC samples
    #if "magDown" in MyOptions:
    #  DaVinci().DDDBtag = 'dddb-20130929'
    #  DaVinci().CondDBtag = 'sim-20121025-vc-md100'
    #
    #if "magUp" in MyOptions:
    #  DaVinci().DDDBtag = 'dddb-20130929-1'
    #  DaVinci().CondDBtag = 'sim-20130522-1-vc-mu100'

# set of detector conditions for data
if "Data" in MyOptions:
  DaVinci().Simulation = False
  DaVinci().Lumi = True
  
  #DaVinci().DDDBtag = 'dddb-20130111'
  #DaVinci().CondDBtag = 'cond-20130114'
  
  if "2011" in MyOptions:
    DaVinci().DataType = '2011'
  
  if "2012" in MyOptions:
    DaVinci().DataType = '2012'

  if "2015" in MyOptions:
    DaVinci().DataType = '2015'
        
  if "2016" in MyOptions:
    DaVinci().DataType = '2016'

  if "2017" in MyOptions:
    DaVinci().DataType = '2017'
        
#some general job options
DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 10000

if "Data" in MyOptions:
  DaVinci().TupleFile = 'bdt_LcD0K.root'

if "MC" in MyOptions:
  DaVinci().TupleFile = 'MC_LcD0K.root'

  #to write variables from all simulated MC signal particles
## DaVinci().UserAlgorithms = [mctpl]
  
################################################################################

# To write a DST output

## if WriteDST :
##   from DSTWriters.microdstelements import *
##   from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements
  
##   # Configuration of SelDSTWriter
##   enablePacking = True
  
##   SelDSTWriterElements = {'default' : stripDSTElements(pack=enablePacking)}
##   SelDSTWriterConf = {'default' : stripDSTStreamConf(pack=enablePacking)}
  
##   dstWriter = SelDSTWriter( "MyDSTWriter",
##                             StreamConf = SelDSTWriterConf,
##                             MicroDSTElements = SelDSTWriterElements,
##                             OutputFileSuffix ='MC12',
##                             SelectionSequences = sc.activeStreams()
##                             )
  
##   DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

################################################################################
# Run on some eos data located at CERN
################################################################################

#importOptions('Lb2LcD0K_MC2012MagDown.py')

if LocalRun :
  
  from Configurables import EventSelector
  
  import sys
  sys.path.append("./")
  
  # get the input files
  from local_samples import getFiles
  EventSelector().Input = getFiles(MyOptions)

################################################################################
