
# run over some local files on the CERN EOS

def getFiles(MyOptions) :

    files = []
    
    if "MC" in MyOptions:
    
        if "2012" in MyOptions:
            
            #MC2012 sample
            eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST'
            
            files = [
                'PFN:{}/00038121/0000/00038121_00000001_2.AllStreams.dst'.format(eos),
                'PFN:{}/00038121/0000/00038121_00000002_2.AllStreams.dst'.format(eos),
                'PFN:{}/00038121/0000/00038121_00000003_2.AllStreams.dst'.format(eos)
                ]

        if "2015" in MyOptions:
            
            #MC2015 sample
            eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST'

            #be careful: this is a Lb2LcD0K MC
            if "magDown" in MyOptions:
                files = [
                    'PFN:{}/00070530/0000/00070530_00000015_6.AllStreams.dst'.format(eos),
                    'PFN:{}/00070530/0000/00070530_00000016_6.AllStreams.dst'.format(eos),
                    'PFN:{}/00070530/0000/00070530_00000029_6.AllStreams.dst'.format(eos)
                    ]
                
        if "2016" in MyOptions:

            #MC2016 sample
            eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST'

            #be careful: this is a Lb2LcD0K MC
            if "magDown" in MyOptions:
                files = [
                    'PFN:{}/00059913/0000/00059913_00000001_7.AllStreams.dst'.format(eos),
                    'PFN:{}/00059913/0000/00059913_00000012_7.AllStreams.dst'.format(eos),
                    'PFN:{}/00059913/0000/00059913_00000016_7.AllStreams.dst'.format(eos)
                    ]
            
    if "Data" in MyOptions:
    
        if "2012" in MyOptions:
        
            #Data 2012 sample, stripping v21r0p1a
            eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/BHADRON.MDST'
            
            files = [
                'PFN:{}/00050929/0000/00050929_00000648_1.bhadron.mdst'.format(eos),
                'PFN:{}/00050929/0000/00050929_00001313_1.bhadron.mdst'.format(eos),
                'PFN:{}/00050929/0000/00050929_00001640_1.bhadron.mdst'.format(eos),
                'PFN:{}/00050929/0000/00050929_00002233_1.bhadron.mdst'.format(eos),
                'PFN:{}/00050929/0000/00050929_00002382_1.bhadron.mdst'.format(eos),
                'PFN:{}/00050929/0000/00050929_00002537_1.bhadron.mdst'.format(eos),
                ]
            
        if "2015" in MyOptions:
        
            #Data 2015 sample, stripping v24r1 (TisTos fix)

            # BHADRON.MDST
            eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/BHADRON.MDST'
            
            files = [
                'PFN:{}/00068487/0000/00068487_00000833_1.bhadron.mdst'.format(eos),
                'PFN:{}/00068487/0000/00068487_00000845_1.bhadron.mdst'.format(eos),
                'PFN:{}/00068487/0000/00068487_00000883_1.bhadron.mdst'.format(eos),
                'PFN:{}/00068487/0000/00068487_00001072_1.bhadron.mdst'.format(eos),
                'PFN:{}/00068487/0000/00068487_00001283_1.bhadron.mdst'.format(eos),
                'PFN:{}/00068487/0000/00068487_00001355_1.bhadron.mdst'.format(eos),
                ]
                                    
            # BHADRONCOMPLETEEVENT.DST
            #eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/BHADRONCOMPLETEEVENT.DST'
            #
            #files = [
            #    'PFN:{}/00068487/0000/00068487_00000454_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00068487/0000/00068487_00000502_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00068487/0000/00068487_00000530_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00068487/0000/00068487_00000531_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00068487/0000/00068487_00000611_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00068487/0000/00068487_00000667_1.bhadroncompleteevent.dst'.format(eos),
            #    ]

        if "2016" in MyOptions:
            
            #Data 2016 sample, stripping v28r1 (TisTos fix)

            # BHADRON.MDST
            eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/BHADRON.MDST'

            files = [
                'PFN:{}/00070271/0000/00070271_00000269_1.bhadron.mdst'.format(eos),
                'PFN:{}/00070271/0000/00070271_00000451_1.bhadron.mdst'.format(eos),
                'PFN:{}/00070271/0000/00070271_00000480_1.bhadron.mdst'.format(eos),
                'PFN:{}/00070271/0000/00070271_00000508_1.bhadron.mdst'.format(eos),
                'PFN:{}/00070271/0000/00070271_00000525_1.bhadron.mdst'.format(eos),
                'PFN:{}/00070271/0000/00070271_00000529_1.bhadron.mdst'.format(eos),
                ]
            
            # BHADRONCOMPLETEEVENT.DST
            #eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/BHADRONCOMPLETEEVENT.DST'
            #
            #files = [
            #    'PFN:{}/00069599/0000/00069599_00000150_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00069599/0000/00069599_00000155_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00069599/0000/00069599_00000158_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00069599/0000/00069599_00000162_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00069599/0000/00069599_00000173_1.bhadroncompleteevent.dst'.format(eos),
            #    'PFN:{}/00069599/0000/00069599_00000189_1.bhadroncompleteevent.dst'.format(eos),
            #    ]
        
        if "2017" in MyOptions:
            
            #Data 2017 sample, stripping v29r2 (VALIDATION!)
            eos = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/validation/Collision17/BHADRONCOMPLETEEVENT.DST'
            
            files = [
                'PFN:{}/00069558/0000/00069558_00000016_1.bhadroncompleteevent.dst'.format(eos),
                'PFN:{}/00069558/0000/00069558_00000032_1.bhadroncompleteevent.dst'.format(eos),
                'PFN:{}/00069558/0000/00069558_00000047_1.bhadroncompleteevent.dst'.format(eos),
                'PFN:{}/00069558/0000/00069558_00000048_1.bhadroncompleteevent.dst'.format(eos),
                'PFN:{}/00069558/0000/00069558_00000075_1.bhadroncompleteevent.dst'.format(eos),
                'PFN:{}/00069558/0000/00069558_00000077_1.bhadroncompleteevent.dst'.format(eos),
                ]

    print "Input files: ", files
    
    return files
