
# if you want to run locally
LocalRun = True

# job options:
if LocalRun:
    MyOptions = ["MC", "MagDown", "2012"]


##############################################################
# set the stripping configuration
print "!!!!!!Job info!!!!!!"
print "MyOptions:"
for i in MyOptions:
    print i


stripping_line = "Lb2LcDKstBeauty2CharmLine"
MC_name = 'Lb2LcDKstBeauty2CharmLine'
decay_data = '(B0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(D0 -> ^K+ ^pi-) ^K-) || (B0 -> ^(Lambda_c~- -> ^p~- ^K+ ^pi-) ^(D0 -> ^K- ^pi+) ^K+)'
decay_MC ='(Lambda_b0 -> ^(Lambda_c+ --> ^p+ ^K- ^pi+) ^(D*(2007)~0 --> ^(D~0 -> ^K+ ^pi-) ^gamma) ^K-) || (Lambda_b~0 -> ^(Lambda_c~- --> ^p~- ^K+ ^pi-) ^(D*(2007)0 --> ^(D0 -> ^K- ^pi+) ^gamma) ^K+)'

tesLoc = ""

if "Data" in MyOptions:
    tesLoc = "Phys/" + stripping_line + "/Particles"  # don't preceed by '/' for uDST
elif "MC" in MyOptions:
    tesLoc = "AllStreams/Phys/" + stripping_line + "/Particles"
else:
    raise NameError('Choose type Data or MC')
    

print "Looking for events in " + tesLoc


################################################################################
# Imports
################################################################################
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *

from Configurables import DaVinci, EventTuple 
from Configurables import FilterDesktop, SubstitutePID

from PhysSelPython.Wrappers import Selection
from PhysSelPython.Wrappers import SelectionSequence
from PhysSelPython.Wrappers import DataOnDemand

from Configurables import LoKi__Hybrid__EvtTupleTool
from Configurables import LoKi__Hybrid__TupleTool

if "MC" in MyOptions:
  from Configurables import MCTupleToolKinematic, MCTupleToolHierarchy, MCDecayTreeTuple

################################################################################
# SubstitutePID
################################################################################

StrippingSels = [DataOnDemand(Location=tesLoc)]

subs=SubstitutePID(
    'MakeKstbar',
    Code="DECTREE('[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC')",
    Substitutions = {
    'Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (Meson -> X+ ^pi-)':'K-',
    'Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (Meson -> ^K+ X-)':'pi+',
    
    'Lambda_b~0 -> (Lambda_c~- -> p~- K+ pi-) (D+ -> K- pi+ pi+) (Meson -> X- ^pi+)':'K+',
    'Lambda_b~0 -> (Lambda_c~- -> p~- K+ pi-) (D+ -> K- pi+ pi+) (Meson -> ^K- X+)':'pi-'

    }
    )


selSub = Selection(
    'SubKstbar_Sel',
    Algorithm=subs,
    RequiredSelections=StrippingSels
    )


selSeq = SelectionSequence('SelSeq', TopSelection=selSub)

map_data = {
    "Lb" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Lc" : "[Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Dm" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^(D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Kst" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) ^(K*(892)0 -> K- pi+)]CC",

    "Lc_p" : "[Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Lc_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Lc_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",

    "Dm_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> ^K+ pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Dm_pim1" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ ^pi- pi-) (K*(892)0 -> K- pi+)]CC",
    "Dm_pim2" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- ^pi-) (K*(892)0 -> K- pi+)]CC",

    "Kst_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> ^K- pi+)]CC",
    "Kst_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K- ^pi+)]CC",
}

map_MC = {
    "Lb" : "(Lambda_b0 -> Lambda_c+ Charm K-) || (Lambda_b~0 -> Lambda_c~- Xc K+)",
    "Lc" : "[Beauty -> ^Lambda_c+ Meson Meson]CC",
    "Dst" : "[Beauty -> Baryon ^Xc Meson]CC",
    "D0" : "[Beauty -> Baryon (D*(2007)~0 -> ^D~0 gamma) Meson]CC",
    #"Lb" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    #"Lc" : "[Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    #"Dm" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^(D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    #"Kst" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) ^(K*(892)0 -> K+ pi-)]CC",

    #"Lc_p" : "[Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    #"Lc_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    #"Lc_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",

    #"Dm_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> ^K+ pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    #"Dm_pim1" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ ^pi- pi-) (K*(892)0 -> K+ pi-)]CC",
    #"Dm_pim2" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- ^pi-) (K*(892)0 -> K+ pi-)]CC",

    #"Kst_K" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> ^K+ pi-)]CC",
    #"Kst_pi" : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-) (K*(892)0 -> K+ ^pi-)]CC",
}



################################################################################
# Config

if "MC" in MyOptions:
  DaVinci().InputType = 'DST'
  #rootInTES = '/Event/'
  #DaVinci(RootInTES=rootInTES, InputType='DST')

if "Data" in MyOptions:
  rootInTES = '/Event/Bhadron'
  DaVinci(RootInTES=rootInTES, InputType='MDST')

print "Input file is " +   DaVinci().InputType
if "Data" in MyOptions:
    print "RootInTES is " + DaVinci().RootInTES
################################################################################
#DecayTreeTuple

def decay_tree_tuple(name, tuple_input, decay, map):
    """Create simple DecayTree"""

    from Configurables import TupleToolDecayTreeFitter
    from Configurables import TupleToolTrackIsolation 
    from Configurables import TupleToolSelResults
    from Configurables import TupleToolProtoPData
    from Configurables import TupleToolTrackInfo
    from Configurables import TupleToolRecoStats
    from Configurables import TupleToolKinematic
    from Configurables import TupleToolGeometry
    from Configurables import TupleToolCaloHypo
    from Configurables import TupleToolTagging
    from Configurables import TupleToolDecay
    from Configurables import TupleToolPid
        
    tpl = DecayTreeTuple('{}DTTuple'.format(name))
    
    tpl.Inputs = [tuple_input]
    tpl.addBranches(map)
    tpl.ToolList = [
        'TupleToolKinematic',
        'TupleToolPid',
        'TupleToolANNPID',
        'TupleToolGeometry',
        'TupleToolPrimaries',
        "TupleToolPropertime",
        "TupleToolRecoStats",

        'TupleToolTrackInfo',
        'TupleToolEventInfo',   #Event infos
        'TupleToolAngles',
        #"LoKi::Hybrid::EvtTupleTool/LoKiEvent"
    ]

    tpl.addTool(TupleToolGeometry,name="TupleToolGeometry")
    tpl.TupleToolGeometry.Verbose = True
    
    tpl.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
    tpl.TupleToolTrackInfo.Verbose = True
    
    tpl.addTool(TupleToolPid,name="TupleToolPid")
    tpl.TupleToolPid.Verbose = True

    #adding more variables to the output ntuple
    LoKiTool = tpl.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
    
    LoKiTool.Variables = {
      "ETA"        : "ETA"
      }
    
#    #list of trigger lines that I want to dump
#    triglist = [
#        
#        #L0 lines
#        "L0CALODecision",
#        "L0DiEM,lowMultDecision",
#        "L0DiHadron,lowMultDecision",
#        "L0DiMuon,lowMultDecision",
#        "L0DiMuonDecision",
#        "L0DiMuonNoSPDDecision",
#        "L0Electron,lowMultDecision",
#        "L0ElectronDecision",
#        "L0ElectronHiDecision",
#        "L0ElectronNoSPDDecision",
#        #"L0GlobalDecision",
#        "L0HadronDecision",
#        "L0HadronNoSPDDecision",
#        "L0HighSumETJetDecision",
#        "L0MUON,minbiasDecision",
#        "L0Muon,lowMultDecision",
#        "L0MuonDecision",
#        "L0MuonNoSPDDecision",
#        "L0Photon,lowMultDecision",
#        "L0PhotonDecision",
#        "L0PhotonHiDecision",
#        "L0PhotonNoSPDDecision",
#
#        #HLT1 lines
#        "Hlt1BeamGasBeam1Decision",
#        "Hlt1BeamGasBeam2Decision",
#        "Hlt1BeamGasCrossingEnhancedBeam1Decision",
#        "Hlt1BeamGasCrossingEnhancedBeam2Decision",
#        "Hlt1BeamGasCrossingForcedRecoDecision",
#        "Hlt1BeamGasCrossingForcedRecoFullZDecision",
#        "Hlt1BeamGasCrossingParasiticDecision",
#        "Hlt1BeamGasHighRhoVerticesDecision",
#        "Hlt1BeamGasNoBeamBeam1Decision",
#        "Hlt1BeamGasNoBeamBeam2Decision",
#        "Hlt1CharmCalibrationNoBiasDecision",
#        "Hlt1DiMuonHighMassDecision",
#        "Hlt1DiMuonLowMassDecision",
#        "Hlt1DiProtonDecision",
#        "Hlt1DiProtonLowMultDecision",
#        "Hlt1ErrorEventDecision",
#        #"Hlt1GlobalDecision",
#        "Hlt1HighPtJetsSinglePVDecision",
#        "Hlt1L0AnyDecision",
#        "Hlt1L0AnyNoSPDDecision",
#        "Hlt1L0HighSumETJetDecision",
#        "Hlt1LumiDecision",
#        "Hlt1LumiMidBeamCrossingDecision",
#        "Hlt1MBMicroBiasTStationDecision",
#        "Hlt1MBMicroBiasVeloDecision",
#        "Hlt1MBNoBiasDecision",
#        "Hlt1NoPVPassThroughDecision",
#        "Hlt1ODINTechnicalDecision",
#        "Hlt1SingleElectronNoIPDecision",
#        "Hlt1SingleMuonHighPTDecision",
#        "Hlt1SingleMuonNoIPDecision",
#        "Hlt1Tell1ErrorDecision",
#        "Hlt1TrackAllL0Decision",
#        "Hlt1TrackAllL0TightDecision",
#        "Hlt1TrackForwardPassThroughDecision",
#        "Hlt1TrackForwardPassThroughLooseDecision",
#        "Hlt1TrackMuonDecision",
#        "Hlt1TrackPhotonDecision",
#        "Hlt1VeloClosingMicroBiasDecision",
#        "Hlt1VertexDisplVertexDecision",
#        
#        #HLT2 lines
#        # Run1 lines
#        "Hlt2Topo2BodyBBDTDecision",
#        "Hlt2Topo2BodySimpleDecision",
#        "Hlt2Topo3BodyBBDTDecision",
#        "Hlt2Topo3BodySimpleDecision",
#        "Hlt2Topo4BodyBBDTDecision",
#        "Hlt2Topo4BodySimpleDecision",
#        "Hlt2TopoE2BodyBBDTDecision",
#        "Hlt2TopoE3BodyBBDTDecision",
#        "Hlt2TopoE4BodyBBDTDecision",
#        "Hlt2TopoMu2BodyBBDTDecision",
#        "Hlt2TopoMu3BodyBBDTDecision",
#        "Hlt2TopoMu4BodyBBDTDecision",
#        "Hlt2TopoRad2BodyBBDTDecision",
#        "Hlt2TopoRad2plus1BodyBBDTDecision",
#        "Hlt2IncPhiDecision",
#        "Hlt2IncPhiSidebandsDecision",
#        
#        # Run 2 lines
#        'Hlt2Topo2BodyDecision',
#        'Hlt2Topo3BodyDecision',
#        'Hlt2Topo4BodyDecision',
#        'Hlt2TopoMu2BodyDecision',
#        'Hlt2TopoMu3BodyDecision',
#        'Hlt2TopoMu4BodyDecision',
#        'Hlt2TopoE2BodyDecision',
#        'Hlt2TopoE3BodyDecision',
#        'Hlt2TopoE4BodyDecision',
#        'Hlt2TopoMuMu2BodyDecision',
#        'Hlt2TopoMuMu3BodyDecision',
#        'Hlt2TopoMuMu4BodyDecision',
#        'Hlt2TopoEE2BodyDecision',
#        'Hlt2TopoEE3BodyDecision',
#        'Hlt2TopoEE4BodyDecision',
#        'Hlt2TopoMuE2BodyDecision',
#        'Hlt2TopoMuE3BodyDecision',
#        'Hlt2TopoMuE4BodyDecision',
#        'Hlt2TopoMuMuDDDecision',
#
#        "Hlt1TrackMVADecision" ,
#        "Hlt1TwoTrackMVADecision"
#        ]

    #adding MC-truth, only for MC samples
    if "MC" in MyOptions:
        print "Adding truth info"
        MCTruth = tpl.addTupleTool("TupleToolMCTruth")
        
        MCTruth.ToolList += [
            'MCTupleToolKinematic',    #kinematic generated variables
            'MCTupleToolHierarchy',    #generated informations about parent and grand-parent
            "MCTupleToolAngles",       #generated decay angles
            #"LoKi::Hybrid::MCTupleTool/LokiTool"   #to add more generated variables in the tuple
            ]
        
        tpl.addTupleTool("TupleToolMCBackgroundInfo")
        
        MCTruth.addTool(MCTupleToolKinematic())
        MCTruth.MCTupleToolKinematic.Verbose = True
  
    #add TIS/TOS trigger variables
    from Configurables import TupleToolTISTOS, TupleToolTrigger, TriggerTisTos

#    #TISTOSTool
#    TISTOSTool = TupleToolTISTOS('TISTOSTool')
#    TISTOSTool.VerboseL0   = True
#    TISTOSTool.VerboseHlt1 = True
#    TISTOSTool.VerboseHlt2 = True
#    TISTOSTool.TriggerList = triglist[:]
#    TISTOSTool.TIS = True
#    TISTOSTool.TOS = True
#    TISTOSTool.TUS = True
#    TISTOSTool.TPS = True
#    TISTOSTool.addTool( TriggerTisTos, name = "TriggerTisTos")
#    
#    tpl.addTool(TISTOSTool, name = "TISTOSTool")
#    tpl.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]
#    
#    #TriggerTool
#    TriggerTool = tpl.addTupleTool("TupleToolTrigger/TriggerTool")
#    TriggerTool.VerboseL0   = True
#    TriggerTool.VerboseHlt1 = True
#    TriggerTool.VerboseHlt2 = True
#    TriggerTool.TriggerList = triglist[:]
    
    tpl.Decay = decay
    return tpl

################################################################################

#MCDecayTreeTuple
def decay_tree_tuple_Full_MC(name, decay):
  
  
  mc_tpl = MCDecayTreeTuple("MCDecayTreeTuple")
  mc_tpl.addBranches(map)
  mc_tpl.ToolList = [
      "MCTupleToolHierarchy",         #generated informations about parent and grand-parent
      #"LoKi::Hybrid::MCTupleTool",    #to add more variables in the tuple
      "TupleToolEventInfo",
      "MCTupleToolAngles"         #generated decay angles
      ]
  
  mc_tpl.addTupleTool("MCTupleToolKinematic").Verbose = True
  
  #add TIS/TOS trigger variables
  from Configurables import TupleToolTrigger, TriggerTisTos


#  #list of trigger lines on which I'm interested
#  triglist = [
#      #L0 lines
#      "L0CALODecision",
#      "L0DiEM,lowMultDecision",
#      "L0DiHadron,lowMultDecision",
#      "L0DiMuon,lowMultDecision",
#      "L0DiMuonDecision",
#      "L0DiMuonNoSPDDecision",
#      "L0Electron,lowMultDecision",
#      "L0ElectronDecision",
#      "L0ElectronHiDecision",
#      "L0ElectronNoSPDDecision",
#      #"L0GlobalDecision",
#      "L0HadronDecision",
#      "L0HadronNoSPDDecision",
#      "L0HighSumETJetDecision",
#      "L0MUON,minbiasDecision",
#      "L0Muon,lowMultDecision",
#      "L0MuonDecision",
#      "L0MuonNoSPDDecision",
#      "L0Photon,lowMultDecision",
#      "L0PhotonDecision",
#      "L0PhotonHiDecision",
#      "L0PhotonNoSPDDecision",
#      
#      #HLT1 lines
#      "Hlt1BeamGasBeam1Decision",
#      "Hlt1BeamGasBeam2Decision",
#      "Hlt1BeamGasCrossingEnhancedBeam1Decision",
#      "Hlt1BeamGasCrossingEnhancedBeam2Decision",
#      "Hlt1BeamGasCrossingForcedRecoDecision",
#      "Hlt1BeamGasCrossingForcedRecoFullZDecision",
#      "Hlt1BeamGasCrossingParasiticDecision",
#      "Hlt1BeamGasHighRhoVerticesDecision",
#      "Hlt1BeamGasNoBeamBeam1Decision",
#      "Hlt1BeamGasNoBeamBeam2Decision",
#      "Hlt1CharmCalibrationNoBiasDecision",
#      "Hlt1DiMuonHighMassDecision",
#      "Hlt1DiMuonLowMassDecision",
#      "Hlt1DiProtonDecision",
#      "Hlt1DiProtonLowMultDecision",
#      "Hlt1ErrorEventDecision",
#      #"Hlt1GlobalDecision",
#      "Hlt1HighPtJetsSinglePVDecision",
#      "Hlt1L0AnyDecision",
#      "Hlt1L0AnyNoSPDDecision",
#      "Hlt1L0HighSumETJetDecision",
#      "Hlt1LumiDecision",
#      "Hlt1LumiMidBeamCrossingDecision",
#      "Hlt1MBMicroBiasTStationDecision",
#      "Hlt1MBMicroBiasVeloDecision",
#      "Hlt1MBNoBiasDecision",
#      "Hlt1NoPVPassThroughDecision",
#      "Hlt1ODINTechnicalDecision",
#      "Hlt1SingleElectronNoIPDecision",
#      "Hlt1SingleMuonHighPTDecision",
#      "Hlt1SingleMuonNoIPDecision",
#      "Hlt1Tell1ErrorDecision",
#      "Hlt1TrackAllL0Decision",
#      "Hlt1TrackAllL0TightDecision",
#      "Hlt1TrackForwardPassThroughDecision",
#      "Hlt1TrackForwardPassThroughLooseDecision",
#      "Hlt1TrackMuonDecision",
#      "Hlt1TrackPhotonDecision",
#    "Hlt1VeloClosingMicroBiasDecision",
#    "Hlt1VertexDisplVertexDecision",
#    
#    #HLT2 lines
#    "Hlt2Topo2BodyBBDTDecision",
#    "Hlt2Topo2BodySimpleDecision",
#    "Hlt2Topo3BodyBBDTDecision",
#    "Hlt2Topo3BodySimpleDecision",
#    "Hlt2Topo4BodyBBDTDecision",
#    "Hlt2Topo4BodySimpleDecision",
#    "Hlt2TopoE2BodyBBDTDecision",
#    "Hlt2TopoE3BodyBBDTDecision",
#    "Hlt2TopoE4BodyBBDTDecision",
#    "Hlt2TopoMu2BodyBBDTDecision",
#    "Hlt2TopoMu3BodyBBDTDecision",
#    "Hlt2TopoMu4BodyBBDTDecision",
#    "Hlt2TopoRad2BodyBBDTDecision",
#    "Hlt2TopoRad2plus1BodyBBDTDecision",
#    "Hlt2IncPhiDecision",
#    "Hlt2IncPhiSidebandsDecision",
#
#    "Hlt1TrackMVADecision" ,
#    "Hlt1TwoTrackMVADecision"
#    ]
#  
#  TriggerTool = mc_tpl.addTupleTool("TupleToolTrigger/TriggerTool")
#  TriggerTool.VerboseL0   = True
#  TriggerTool.VerboseHlt1 = True
#  TriggerTool.VerboseHlt2 = True
#  TriggerTool.TriggerList = triglist[:]
  mc_tpl.Decay = decay

  return mc_tpl

################################################################################
# Make selection
################################################################################
name = 'Lb2LcDKst'
tuple_input = ""
decay = ""
map = ""
# selection sequence when you don't want to restrip

if "MC" in MyOptions:
    tuple_input = tesLoc
    decay = decay_MC
    map = map_MC

if "Data" in MyOptions:
    tuple_input = selSeq.outputLocation()
    decay = decay_data
    map = map_data

# make the tuple
tpl = decay_tree_tuple(name, tuple_input, decay, map)

# tuple with informations from MC generated signals
if "MC" in MyOptions:
  mctpl = decay_tree_tuple_Full_MC(name, decay)

################################################################################
#
################################################################################

# Add decay tree fitter
from Configurables import TupleToolDecayTreeFitter,TupleToolDecay

tpl.Lb.addTupleTool("TupleToolDecayTreeFitter/Cons")
tpl.Lb.Cons.constrainToOriginVertex = True
tpl.Lb.Cons.Verbose = True
tpl.Lb.Cons.daughtersToConstrain = [ "Lambda_c+","D-" ]


################################################################################
seq = GaudiSequencer('My{}DTTupleSeq'.format(name))

if "Data" in MyOptions:
    seq.Members += [selSeq.sequence()]

seq.Members += [tpl]

print "Added tpl "
#add the MCDecayTreeTuple
if "MC" in MyOptions:
    #seq.Members += [mctpl]
    print "Added mctpl "
  


################################################################################

# set of detector conditions for MC
if "MC" in MyOptions:
  DaVinci().Simulation = True
  DaVinci().Lumi = False

  if "2016" in MyOptions:
      DaVinci().DataType = '2016'     
      DaVinci().DDDBtag = 'dddb-20170721-3'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20170721-2-vc-md100'          
      elif "MagUp" in MyOptions:
          DaVinci().CondDBtag = 'sim-20170721-2-vc-mu100'
      else :
        raise NameError('Choose MagDown or MagUp')
            
              
  elif "2015" in MyOptions:
      DaVinci().DataType = '2015'      
      DaVinci().DDDBtag = 'dddb-20170721-3'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20161124-vc-md100'          
      elif "MagUp" in MyOptions:
          DaVinci().CondDBtag = 'sim-20161124-vc-mu100'
      else :
        raise NameError('Choose MagDown or MagUp')
                              
  elif "2012" in MyOptions:
      
      DaVinci().DataType = '2012'      
      DaVinci().DDDBtag = 'dddb-20170721-2'
    
      if "MagDown" in MyOptions:
         DaVinci().CondDBtag = 'sim-20160321-2-vc-md100'         
      elif "MagUp" in MyOptions:             
              DaVinci().CondDBtag = 'sim-20160321-2-vc-mu100'              
      else :
        raise NameError('Choose MagDown or MagUp')        
    
  elif "2011" in MyOptions:
      
      DaVinci().DataType = '2011'      
      DaVinci().DDDBtag = 'dddb-20170721-1'
      
      if "MagDown" in MyOptions:
          DaVinci().CondDBtag = 'sim-20160614-1-vc-md100'         
      elif "MagUp" in MyOptions:
              DaVinci().CondDBtag = 'sim-20160614-1-vc-mu100'             
      else :
        raise NameError('Choose MagDown or MagUp')        

  else:
      raise NameError('Choose 20XX as Datatype')
  
  print "Datatype" + DaVinci().DataType
  print "DDDBtag" + DaVinci().DDDBtag
  print "CondDBtag" + DaVinci().CondDBtag

# set of detector conditions for data
if "Data" in MyOptions:
  DaVinci().Simulation = False
  DaVinci().Lumi = True
  
  if "2011" in MyOptions:
    DaVinci().DataType = '2011'
  
  elif "2012" in MyOptions:
    DaVinci().DataType = '2012'

  elif "2015" in MyOptions:
    DaVinci().DataType = '2015'
        
  elif "2016" in MyOptions:
    DaVinci().DataType = '2016'

  elif "2017" in MyOptions:
    DaVinci().DataType = '2017'

  elif "2018" in MyOptions:
    DaVinci().DataType = '2018'

  else:
    raise NameError('Choose 20XX as Datatype')
  
  print "Datatype" + DaVinci().DataType
  
#some general job options
DaVinci().EvtMax = 100000
DaVinci().SkipEvents = 0
#DaVinci().ErrorMax = 100
DaVinci().PrintFreq = 10000

DaVinci().appendToMainSequence([seq])

if "Data" in MyOptions:
  DaVinci().TupleFile = 'Lb2LcDKst.root'

if "MC" in MyOptions:   
    DaVinci().TupleFile = 'MC_Lb2LcDKst.root'

    #to write variables from all simulated MC signal particles
    DaVinci().UserAlgorithms = [mctpl]

print "Making " + DaVinci().TupleFile
print "!!!!!!Job info!!!!!!"


################################################################################
# Run on some eos data located at CERN
################################################################################



if LocalRun :
  if "MC" in MyOptions:  
      EventSelector().Input = [
          "DATAFILE='00051043_00000034_2.AllStreams.dst'  TYP='POOL_ROOTTREE' Opt='READ'",
          ]
  if "Data" in MyOptions:
      DaVinci().Input = ['../../../../B2DK_D2KKpipi/Gamma_from_B2DK_D2KKpipi/scripts/Davinci/data/00069527_00000006_1.bhadron.mdst']
      
  
################################################################################
