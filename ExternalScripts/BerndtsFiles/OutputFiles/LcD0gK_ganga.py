# for this to work you need to check out this repository:
# https://gitlab.cern.ch/sneubert/DfromBBDTs
# make sure you chave yuor eos space configured:  https://github.com/lhcb/first-analysis-steps/blob/master/09-eos-storage.md

#choose
#period = '2012MagDown'
#period = '2012MagUp'
#period = '2011MagDown'
#period = '2011MagUp'

#period = 'MC2012MagDown'
#period = 'MC2012MagUp'
#period = 'MC2011MagDown'
#period = 'MC2011MagUp'

def prepareJob(period='MC2012MagDown',nfiles=None):

    # get data set
    bkk_paths = {'MC2016MagDown' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST',
                 'MC2016MagUp'   : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST',
                 'MC2015MagDown' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST',
                 'MC2015MagUp'   : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST',
                 'MC2012MagDown' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15396200/ALLSTREAMS.DST',
                 'MC2012MagUp'   : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15396200/ALLSTREAMS.DST',
                 'MC2011MagDown' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST',  
                 'MC2011MagUp'   : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST'}

    dataset = BKQuery(bkk_paths[period]).getDataset()
    
    j=Job()
    j.name = 'LcD0gK_' + period
    #dvversion = 'v42r6p1'
    #if "MC2016" in period or "MC2015" in period:
    #    dvversion = 'v42r6p1'
    
    opts = []
    
    opts.append('setup'+period+'D0g.py');
    opts.append('mvaLb2LcD0K_nostripping.py')

    # #generic DaVinci installation, mainly to run over data
    application_directory = "/eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/code_analysis/DaVinci_v50r0_2"
    platform = "x86_64-slc6-gcc62-opt"
    
    # #specific DaVinci installations, to re-run the stripping over the MC
    # if "MC2016" in period or "MC2015" in period:
    #     application_directory = "/eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/code_analysis/DaVinci_v41r4p4"
    #     platform = "x86_64-slc6-gcc49-opt"
        
    #if "MC2015" in period: #originally commented out, not my edit (bmumme)
    #    application_directory = "/eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/code_analysis/DaVinci_v38r1p6"
    #    platform = "x86_64-slc6-gcc49-opt"

    # In a try to make the GaudiExec look more like the Starterkit
    # #new lb-* formalism
    myApp = GaudiExec()
    myApp.options = opts
    myApp.directory = application_directory

    #myApp = prepareGaudiExec('DaVinci', 'v50r0', myPath='.')
    #
    #platform = "x86_64-slc6-gcc62-opt"
    j.application = myApp
   #j.application.options = opts
    j.application.platform = platform
    
    #old SetupProject formalism
    #j.application = DaVinci(version='v42r2')
    #j.application.platform='x86_64-slc6-gcc49-opt'
    #j.application.optsfile = opts
    
    j.inputdata=dataset[0:nfiles]
    
    j.backend = Dirac()
    #j.backend = LSF( queue = '1')
    
    j.splitter = SplitByFiles(filesPerJob = 40, ignoremissing = True)

    # upload BDT weights
    #j.inputfiles = ['externals/DfromBBDTs/weights/TMVAnalysis_D_BDT_V_NC.weights.xml' ,
    #                  'externals/DfromBBDTs/weights/TMVAnalysis_Ds_BDT_V_NC.weights.xml',
    #                  'externals/DfromBBDTs/weights/TMVAnalysis_D0_BDT_NC.weights.xml'  ,
    #                  'externals/DfromBBDTs/weights/TMVAnalysis_Lc_BDT_NC.weights.xml'  ,]
    
    # configure output location
    # this requires eos to be setup! 

    output_name = ""
    

    output_name = 'bdt_LcD0K.root'

    if ('MC' in period) :
        output_name = 'MC_LcD0K.root'
        
    j.outputfiles = [MassStorageFile ( compressed = False ,
                                       namePattern = output_name ,
                                       outputfilenameformat = 'Lb2LcD0gK/'+period+'/{jid}_{sjid}_{fname}')]
    

    return j
