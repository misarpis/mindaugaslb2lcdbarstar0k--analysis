# write the PFNs of the output files for a given job, in an output bash file
def write_pfns(job_num, outDir, outFile_protoname, outFile_name):

    job = jobs(job_num)
    
    subjobs = job.subjobs

    if len(subjobs) == 0:
        subjobs = [job]

    pfns = {}
    
    for j in subjobs:
        if j.status != 'completed':
            print 'Skipping #{0}'.format(j.id)

            f_failing = open("failing_" + str(job_num) + "_" + str(j.id)  + ".py", 'w')

            # open the file
            f_failing.write("files = [\n")
            
            # build a dictionary of lists: key = subjobs ID, [file PFN, output location]
            for df in j.inputdata.files.get(DiracFile):
                f_failing.write('\t"' + (df.accessURL())[0] + '",\n')

            # close the file
            f_failing.write("]")
            
        else :
            # build a dictionary of lists: key = subjobs ID, [file PFN, output location]
            for df in j.outputfiles.get(DiracFile):
                pfns[j.id] = [(df.accessURL())[0], outDir + "/" + str(job_num) + "_" + str(j.id) + "_" + outFile_protoname ]

    f = open(outFile_name, 'w')

    # build the command to copy the output files in the output location,
    # using the xrootd protocol
    for subjob_id in pfns.keys() :
        f.write("xrdcp " + pfns[subjob_id][0] + " " + pfns[subjob_id][1] + "\n")

# get all files in a single shot
def get_all_files() :
    
    #write_pfns(19, "/eos/lhcb/user/m/misarpis/ganga/Lb2LcD0K/2018MagUp", "LcD0K.root", "copy_2018MagUp_Lb2LcD0K.sh")
    write_pfns(19, "/eos/lhcb/user/m/misarpis/ganga/Lb2LcD0K/2018MagDown",   "LcD0K.root", "copy_2018MagDown_Lb2LcD0K.sh")
    
    return
